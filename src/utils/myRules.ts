// 手机号验证
function telphone(rule, value, callback) {
  if (value === "") {
    callback();
    return;
  }
  const pass = /^(0[0-9]{2,3}-)?([2-9][0-9]{6,7})+(-[0-9]{1,4})?$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "正确格式: 区号-电话号码 或 电话号码"));
  }
  callback();
}
// 电话号验证
function phone(rule, value, callback) {
  if (value === "") {
    callback(new Error(rule.message || "请输入电话号码"));
    return;
  }
  const pass = /^(\\+86)?0?[1][34578][0-9]{9}$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "请输入正确的电话号码"));
  }
  callback();
}
// 身份证验证
function idNo(rule, value, callback) {
  if (value === "") {
    callback(new Error(rule.message || "请输入身份证号码"));
    return;
  }
  const pass = /^[1-9]\d{5}(18|19|20)\d{2}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])\d{3}([0-9]|X)$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "请输入正确的身份证号码"));
  }
  callback();
}
// 邮箱验证
function email(rule, value, callback) {
  if (value === "") {
    callback();
    return;
  }
  const pass = /^[a-zA-Z0-9_.-]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]{2,4}$/g.test(
    value
  );
  if (!pass) {
    callback(new Error(rule.message || "请输入正确的邮箱"));
  }
  callback();
}
// 6位数字验证
function postcode(rule, value, callback) {
  if (value === "") {
    callback();
    return;
  }
  const pass = /^\d{6}$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "必须是6位数字"));
  }
  callback();
}
// 不能包含特殊字符验证
function notValidInputText(rule, value, callback) {
  if (value === "") {
    callback();
    return;
  }
  const pass = /^[^'/\\"()@$%^*<>&?]*$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "不能包含特殊字符^'\\\"()@$%^*<>&?*"));
  }
  callback();
}
// 只能是数组验证
function isNumber(rule, value, callback) {
  if (value === "") {
    callback();
    return;
  }
  const pass = /^[0-9]*$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "只能为数字"));
  }
  callback();
}
// ip地址验证
function ip(rule, value, callback) {
  if (value === "") {
    callback();
    return;
  }
  const pass =
    /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/g.test(
      value
    );
  if (!pass) {
    callback(new Error(rule.message || "请输入正确的ip地址"));
  }
  callback();
}
// 只能是数字和字母验证
function letterOrNumber(rule, value, callback) {
  if (value === "") {
    callback();
    return;
  }
  const pass = /^[0-9a-zA-Z]+$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "只能是数字和字母"));
  }
  callback();
}
// 只能是数字和字母和_ 验证
function letterOrNumberorUl(rule, value, callback) {
  if (value === "") {
    callback();
    return;
  }
  const pass = /^\w+$/.test(value);
  if (!pass) {
    callback(new Error(rule.message || "只能是数字和字母和_"));
  }
  callback();
}
// 只能为 3-20 位字符串验证
function id(rule, value, callback) {
  if (value === "") {
    callback();
    return;
  }
  const pass = /^[a-zA-Z0-9-]{3,20}$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "只能为 3-20 位字符串"));
  }
  callback();
}
// 必须是数字验证
function number(rule, value, callback) {
  if (value === "") {
    callback();
    return;
  }
  const pass = /^[0-9]{1,10}$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "必须是数字"));
  }
  callback();
}
// 必须是正整数验证
function positiveNumber(rule, value, callback) {
  if (!value) {
    callback();
    return;
  }
  const pass = /^[0-9]\d*$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "必须是正整数"));
  }
  callback();
}
// 可输入正数负数和小数验证
function posiNactiveNumber(rule, value, callback) {
  if (value === "") {
    callback(new Error(rule.message || "不能为空"));
    return;
  }
  const pass = /^[+-]?\d+(\.\d+)?$/g.test(value);
  if (!pass) {
    // callback(new Error(rule.message || '可输入正数负数和小数'))
    callback(new Error("请输入整数或者小数"));
  }
  callback();
}
function tradePriceV(rule, value, callback) {
  if (!value) {
    return callback(new Error("进价不能为空"));
  }
  const pass = /^\d+(\.\d+)?$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "只能输入数字"));
  }
  callback();
}
function retailPriceV(rule, value, callback) {
  if (!value) {
    return callback(new Error("售价不能为空"));
  }
  const pass = /^\d+(\.\d+)?$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "只能输入数字"));
  }
  callback();
}
// 入库数量
function quantityNumber(rule, value, callback) {
  if (!value) {
    callback(new Error(rule.message || "入库数量不能为空"));
    return;
  }
  const pass = /^[0-9]\d*$/g.test(value);
  if (!pass) {
    callback(new Error(rule.message || "必须是正整数"));
  }
  callback();
}

export {
  telphone,
  phone,
  email,
  postcode,
  notValidInputText,
  isNumber,
  ip,
  letterOrNumber,
  letterOrNumberorUl,
  id,
  number,
  positiveNumber,
  posiNactiveNumber,
  tradePriceV,
  retailPriceV,
  quantityNumber,
  idNo,
};
