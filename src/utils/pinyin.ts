import { pinyin } from "pinyin-pro";
/**
 * 拼音码
word：必填。String 类型，需要转化为拼音的中文
options：可选。Object 类型，用于配置各种输出形式，options 的键值配置如下：

参数                说明                                                                类型      可选值                                      默认值
pattern             输出的结果的信息 (拼音/声母/母/音/首字母)                           string      pinyin / initial / final / num / first      pinyin
toneType            音调输出形式(拼音符号/数字/不加音调)                                string      symbol / num / none                         symbol
type                输出结果类型 (字符串/数组)                                         string      string / array                              string
multiple            输出多音字全部拼音 (仅在 word 为长度为 1的汉字字符串时生效)          boolean      true / false                                false
mode                拼音查找的模式(常规模式/姓氏模式)                                   string      normal / surname                            normal

 * 
 */

/**
 * 去除字符串中的空格
 * @param params 字符串
 * @returns
 */
function deleteSpace(params: string): string {
  return params.replace(/\s*/g, "");
}
/**
 * 获取第一个字母
 * @param params 字符串
 */
function firstLetterResult(params: string): string {
  return deleteSpace(
    pinyin(params, {
      pattern: "first",
      toneType: "none",
      type: "string",
    }).toUpperCase()
  );
}

/**
 * 获取全拼音
 * @param params 字符串
 */
function allLetterResult(params: string): string {
  return deleteSpace(
    pinyin(params, {
      pattern: "pinyin",
      toneType: "none",
      type: "string",
    }).toUpperCase()
  );
}

export { firstLetterResult, allLetterResult };
