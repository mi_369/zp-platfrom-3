/**
 * 自动补全表头公共js
 */
/**
 * json字符串格式化为json对象
 * @param str json字符串
 * @returns {any|null}
 */
function jsonStrInObj(str: string): any | null {
  console.log("自动补全表格表头", str);
  if (typeof str == "string" && str.length > 0) {
    return JSON.parse(str);
  }
  return null;
}
/** 诊疗项目对照自动补全 */
const clinicTableHeadList =
  "[" +
  "                {" +
  '                    "label": "项目分类",' +
  '                    "prop": "itemClass",' +
  '                    "width": "84",' +
  '                    "dictType": "bill_item_class_dict",' +
  '                    "class": "dict",' +
  '                    "align": "left"' +
  "                }," +
  '                {"label": "项目代码", "prop": "itemCode", "width": "160"},' +
  '                {"label": "项目名称", "prop": "itemName", "width": "260"},' +
  '                {"label": "总价(元)", "prop": "subTotal", "width": "120"}' +
  "            ]";

/** 价表对照自动补全 */
const priceTableHeadList =
  "[" +
  "                {" +
  '                    "label": "项目分类",' +
  '                    "prop": "itemClass",' +
  '                    "width": "80",' +
  '                    "dictType": "bill_item_class_dict",' +
  '                    "class": "dict",' +
  '                    "align": "left"' +
  "                }," +
  '                {"label": "项目代码", "prop": "itemCode", "width": "110"},' +
  '                {"label": "项目名称", "prop": "itemName", "width": "180"},' +
  '                {"label": "项目单价(元)", "prop": "price", "width": "90"},' +
  '                {"label": "项目规格", "prop": "itemSpec", "width": "90"},' +
  '                {"label": "项目单位", "prop": "units", "width": "70", "dictType": "spec_unit", "class": "dict"}' +
  "            ]";

/** 药品入库自动补全 */
const drugImporOrExportList =
  "[" +
  "                {" +
  '                    "label": "药品名称",' +
  '                    "prop": "drugName",' +
  '                    "width": "140",' +
  '                    "dictType": "bill_item_class_dict",' +
  '                    "align": "left"' +
  "                }," +
  '                {"label": "药品编码", "prop": "drugCode", "width": "152"},' +
  '                {"label": "包装规格", "prop": "packageSpec", "width": "90"},' +
  '                {"label": "包装单位", "prop": "units", "class":"dict", "dictType": "spec_unit", "width": "80"},' +
  '                {"label": "进货价(元)", "prop": "tradePrice", "width": "90"},' +
  '                {"label": "零售价(元)", "prop": "retailPrice", "width": "90"},' +
  '                {"label": "厂商", "prop": "firmName", "width": "150"},' +
  '                {"label": "包装数量", "prop": "packageNum", "width": "90"}' +
  "            ]";
/** icd10自动补全 */
const icd10List =
  "[" +
  "                {" +
  '                    "label": "诊断编码",' +
  '                    "prop": "code",' +
  '                    "width": "240",' +
  '                    "align": "left"' +
  "                }," +
  '                {"label": "诊断名称", "prop": "zhongwenMingcheng", "width": "300"},' +
  '                {"label": "诊断类型", "prop": "diagnosisType", "width": "170"},' +
  '                {"label": "中/西医诊断", "prop": "chineseWesternType",  "dictType": "chinese_western_type", "width": "163"}' +
  "            ]";

export {
  jsonStrInObj,
  clinicTableHeadList,
  priceTableHeadList,
  drugImporOrExportList,
  icd10List,
};
