import { defineStore } from "pinia";

export const useTagsViewStore = defineStore("tagsView", () => {
  const statevisited = {
    path: "/index",
    fullPath: "/index",
    name: "Index",
    title: "首页",
    affix: false,
    keepAlive: false,
  };
  let visitedViews = reactive<TagView[]>([statevisited]);
  let cachedViews = reactive<string[]>([]);
  let iframeViews = reactive<string[]>([]);

  function ADD_IFRAME_VIEW(view) {
    if (iframeViews.some((v: any) => v.path === view.path)) return;
    iframeViews.push(
      Object.assign({}, view, {
        title: view.meta.title || "no-name",
      })
    );
  }
  function ADD_VISITED_VIEW(view) {
    if (visitedViews.some((v) => v.path === view.path)) return;
    visitedViews.push(
      Object.assign({}, view, {
        title: view.meta["title"] || "no-name",
      })
    );
  }
  function ADD_CACHED_VIEW(view) {
    if (cachedViews.includes(view.name)) return;
    if (view.meta && !view.meta.noCache) {
      cachedViews.push(view.name);
    }
  }
  function DEL_OTHERS_VISITED_VIEWS(view) {
    visitedViews = visitedViews.filter((v: any) => {
      return v.meta.affix || v.path === view.path;
    });

    iframeViews = iframeViews.filter((item: any) => item.path === view.path);
  }
  function DEL_OTHERS_CACHED_VIEWS(view) {
    const index = cachedViews.indexOf(view.name);
    if (index > -1) {
      cachedViews = cachedViews.slice(index, index + 1);
    } else {
      cachedViews = [];
    }
  }
  /** 关闭左侧 */
  function delLeftViews(view: TagView) {
    return new Promise((resolve) => {
      const currIndex = visitedViews.findIndex((v) => v.path === view.path);
      if (currIndex === -1) {
        return;
      }
      for (const [i, v] of visitedViews.entries()) {
        if (i < currIndex && v.path !== "/index") {
          visitedViews.splice(i, 1);
        }
      }
      resolve({
        visitedViews: [...visitedViews],
      });
    });
  }
  function addView(view) {
    addVisitedView(view);
    addCachedView(view);
  }
  function addVisitedView(view) {
    ADD_VISITED_VIEW(view);
  }
  function addCachedView(view) {
    ADD_CACHED_VIEW(view);
  }
  function addIframeView(view) {
    ADD_IFRAME_VIEW(view);
  }
  /** 关闭当前 */
  function delView(view) {
    return new Promise((resolve) => {
      delVisitedView(view);
      delCachedView(view);
      resolve({
        visitedViews: [...visitedViews],
        cachedViews: [...cachedViews],
      });
    });
  }
  /** 关闭其他 */
  function delOtherViews(view: TagView) {
    return new Promise((resolve) => {
      delOtherVisitedViews(view);
      delOtherCachedViews(view);
      resolve({
        visitedViews: [...visitedViews],
        cachedViews: [...cachedViews],
      });
    });
  }
  /** 关闭其他视图被调用 */
  function delOtherVisitedViews(view: TagView) {
    return new Promise((resolve) => {
      for (const [i, v] of visitedViews.entries()) {
        if (v.path !== view.path && v.path !== "/index") {
          visitedViews.splice(i, 1);
        }
      }
      resolve([...visitedViews]);
    });
  }
  /** 关闭其他缓存被调用 */
  function delOtherCachedViews(view: TagView) {
    const viewName = view.name as string;
    return new Promise((resolve) => {
      const index = cachedViews.indexOf(viewName);
      if (index > -1) {
        cachedViews = cachedViews.slice(index, index + 1);
      } else {
        cachedViews = [];
      }
      resolve([...cachedViews]);
    });
  }

  function delVisitedView(view) {
    return new Promise((resolve) => {
      for (const [i, v] of visitedViews.entries()) {
        // 找到与指定视图路径匹配的视图，在已访问视图列表中删除该视图
        if (v.path === view.path) {
          visitedViews.splice(i, 1);
          break;
        }
      }
      resolve([...visitedViews]);
    });
  }
  /** 删除缓存视图 */
  function delCachedView(view) {
    const viewName = view.name;
    return new Promise((resolve) => {
      const index = cachedViews.indexOf(viewName);
      index > -1 && cachedViews.splice(index, 1);
      resolve([...cachedViews]);
    });
  }

  function delOthersVisitedViews(view) {
    return new Promise((resolve) => {
      DEL_OTHERS_VISITED_VIEWS(view);
      resolve([...visitedViews]);
    });
  }
  function delOthersCachedViews(view) {
    return new Promise((resolve) => {
      DEL_OTHERS_CACHED_VIEWS(view);
      resolve([...cachedViews]);
    });
  }
  /** 关闭全部 */
  function delAllViews() {
    return new Promise((resolve) => {
      let newVisitedViews: any[] = visitedViews;
      while (visitedViews.length > 0) {
        visitedViews.splice(0, 1);
      }
      visitedViews.push(statevisited);
      cachedViews = [];
      resolve({
        visitedViews: [...visitedViews],
        cachedViews: [...cachedViews],
      });
    });
  }
  /** 修改视图 */
  function updateVisitedView(view) {
    for (let v of visitedViews) {
      if (v.path === view.path) {
        v = Object.assign(v, view);
        break;
      }
    }
  }
  /** 关闭右侧 */
  function delRightViews(view) {
    return new Promise((resolve) => {
      const currIndex = visitedViews.findIndex((v) => v.path === view.path);
      if (currIndex === -1) {
        return;
      }
      for (const [i, v] of visitedViews.entries()) {
        if (i > currIndex && v.path !== "/index") {
          visitedViews.splice(i, 1);
        }
      }
      resolve({
        visitedViews: [...visitedViews],
      });
    });
  }
  return {
    addView,
    addIframeView,
    addVisitedView,
    addCachedView,
    delView,
    delVisitedView,
    delOthersVisitedViews,
    delOthersCachedViews,
    updateVisitedView,
    visitedViews,
    cachedViews,
    iframeViews,
    delCachedView,
    delOtherViews,
    delLeftViews,
    delRightViews,
    delAllViews,
  };
});
