import Cookies from "js-cookie";
import { defineStore } from "pinia";
import { useStorage } from "@vueuse/core";
import defaultSettings from "@/settings";
import zhCn from "element-plus/es/locale/lang/zh-cn";
import en from "element-plus/es/locale/lang/en";

export const useAppStore = defineStore("app", () => {
  // state
  let device = useStorage("device", "desktop");
  let size = useStorage<any>("size", defaultSettings.size);
  let language = useStorage("language", defaultSettings.language);

  const sidebarStatus = useStorage("sidebarStatus", "1");

  const sidebar = reactive({
    hide: false,
    opened: sidebarStatus.value === "1",
    withoutAnimation: false,
  });
  const activeTopMenu = useStorage("activeTop", "");
  /**
   * 根据语言标识读取对应的语言包
   */
  const locale = computed(() => {
    if (language?.value == "en") {
      return en;
    } else {
      return zhCn;
    }
  });

 /**
   * 混合模式顶部切换
   */
  function toggleSideBar(type :string) {
// 判断是否是第一次登录，true 设置左侧菜单列表默认折叠
    if (type === "start") {
      sidebar.opened = true
      sidebarStatus.value = "1";
      sidebar.withoutAnimation = false;
      return
    }
    sidebar.opened = !sidebar.opened;
    sidebar.withoutAnimation = false;
    if (sidebar.opened) {
      sidebarStatus.value = "1";
    } else {
      sidebarStatus.value = "0";
    }
  }

  // function closeSideBar(withoutAnimation: boolean) {
  //   sidebar.opened = false;
  //   sidebar.withoutAnimation = withoutAnimation;
  //   sidebarStatus.value = "closed";
  // }

  function openSideBar(withoutAnimation: boolean) {
    sidebar.opened = true;
    sidebar.withoutAnimation = withoutAnimation;
    sidebarStatus.value = "0";
  }

  // function toggleDevice(val: string) {
  //   device.value = val;
  // }

  function changeSize(val: string) {
    size.value = val;
  }
  /**
   * 切换语言
   *
   * @param val
   */
  function changeLanguage(val: string) {
    language.value = val;
  }
  /**
   * 混合模式顶部切换
   */
  function changeTopActive(val: string) {
    activeTopMenu.value = val;
  }
  function CLOSE_SIDEBAR(withoutAnimation) {
    Cookies.set("sidebarStatus", "0");
    sidebar.opened = false;
    sidebar.withoutAnimation = withoutAnimation;
  }
  function TOGGLE_DEVICE(device) {
    device = device;
  }
  function SET_SIZE(sizeInfo) {
    size = sizeInfo;
    Cookies.set("size", sizeInfo);
  }
  function SET_SIDEBAR_HIDE(status) {
    sidebar.hide = status;
  }
  // function toggleSideBar() {
  //   TOGGLE_SIDEBAR();
  // }
    // function TOGGLE_SIDEBAR() {
  //   if (sidebar.hide) {
  //     return false;
  //   }
  //   sidebar.opened = !sidebar.opened;
  //   sidebar.withoutAnimation = false;
  //   if (sidebar.opened) {
  //     Cookies.set("sidebarStatus", String(0));
  //   } else {
  //     Cookies.set("sidebarStatus", String(0));
  //   }
  // }
  function closeSideBar({ withoutAnimation }) {
    CLOSE_SIDEBAR(withoutAnimation);
  }
  function toggleDevice(device) {
    TOGGLE_DEVICE(device);
  }
  function setSize(size) {
    SET_SIZE(size);
  }
  function toggleSideBarHide(status) {
    SET_SIDEBAR_HIDE(status);
  }
  return {
    device,
    sidebar,
    language,
    locale,
    size,
    activeTopMenu,
    toggleDevice,
    changeLanguage,
    toggleSideBar,
    closeSideBar,
    openSideBar,
    changeTopActive,
    toggleSideBarHide,
    changeSize,
  };
});
