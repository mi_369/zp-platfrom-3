import auth from "@/plugins/auth";
import router, { constantRoutes, dynamicRoutes } from "@/router";
import { getRouters } from "@/api/system/menu";
import Layout from "@/layout/index.vue";
import ParentView from "@/components/ParentView/index.vue";
import InnerLink from "@/layout/components/InnerLink/index.vue";
import { RouteRecordRaw } from "vue-router";
import { defineStore } from "pinia";
import { store } from "@/store";

//@ts-ignore
// 匹配views里面所有的.vue文件
const modules = import.meta.glob("../../views/**/**.vue");
// const router = useRouter();

export const usePermissionStore = defineStore("permission", () => {
  let routes = ref<RouteRecordRaw[]>([]);
  let addRoutes = ref<RouteRecordRaw[]>([]);
  let defaultRoutes = ref<RouteRecordRaw[]>([]);
  let topbarRouters = ref<RouteRecordRaw[]>([]);
  let sidebarRouters = ref<RouteRecordRaw[]>([]);

  function SET_ROUTES(routesInfo) {
    addRoutes = routesInfo;
    routes.value = constantRoutes.concat(routesInfo);
  }
  function SET_DEFAULT_ROUTES(routes) {
    defaultRoutes.value = constantRoutes.concat(routes);
  }
  function SET_TOPBAR_ROUTES(routes) {
    topbarRouters.value = routes;
  }
  function SET_SIDEBAR_ROUTERS(routesInfo) {
    sidebarRouters.value = routesInfo;
    routes.value = routesInfo;
  }
  // 生成路由
  function GenerateRoutes(_val) {
    return new Promise((resolve) => {
      // 向后端请求路由数据
      getRouters().then((res) => {
        const sdata = JSON.parse(JSON.stringify(res.data));
        const rdata = JSON.parse(JSON.stringify(res.data));
        const sidebarRoutes = filterAsyncRouter(sdata);
        const rewriteRoutes = filterAsyncRouter(rdata, false, true);
        const asyncRoutes = filterDynamicRoutes(dynamicRoutes);
        rewriteRoutes.push({ path: "*", redirect: "/404", hidden: true });
        //向路由表中增加 router 中定义好的动态路由
        asyncRoutes.forEach((item) => {
          router.addRoute(item);
        });
        SET_SIDEBAR_ROUTERS(constantRoutes.concat(sidebarRoutes));
        resolve(sidebarRouters);
      });
    });
  }
  // 遍历后台传来的路由字符串，转换为组件对象
  function filterAsyncRouter(asyncRouterMap, lastRouter = false, type = false) {
    return asyncRouterMap.filter((route) => {
      if (type && route.children) {
        route.children = filterChildren(route.children);
      }
      if (route.component) {
        // Layout ParentView 组件特殊处理
        if (route.component === "Layout") {
          route.component = Layout;
        } else if (route.component === "ParentView") {
          route.component = ParentView;
        } else if (route.component === "InnerLink") {
          route.component = InnerLink;
        } else {
          route.component = loadView(route.component);
        }
      }
      if (route.children != null && route.children && route.children.length) {
        route.children = filterAsyncRouter(route.children, route, type);
      } else {
        delete route["children"];
        delete route["redirect"];
      }
      return true;
    });
  }
  function filterChildren(childrenMap, lastRouter = false) {
    let children = <any>[];
    childrenMap.forEach((el, index) => {
      if (el.children && el.children.length) {
        if (el.component === "ParentView" && !lastRouter) {
          el.children.forEach((c) => {
            c.path = el.path + "/" + c.path;
            if (c.children && c.children.length) {
              children = children.concat(filterChildren(c.children, c));
              return;
            }
            children.push(c);
          });
          return;
        }
      }
      if (lastRouter) {
        el.path = lastRouter["path"] + "/" + el.path;
      }
      children = children.concat(el);
    });
    return children;
  }
  // 动态路由遍历，验证是否具备权限
  function filterDynamicRoutes(routes) {
    const res = <any>[];
    routes.forEach((route) => {
      if (route.permissions) {
        if (auth.hasPermOr(route.permissions)) {
          res.push(route);
        }
      } else if (route.roles) {
        if (auth.hasRoleOr(route.roles)) {
          res.push(route);
        }
      }
    });
    return res;
  }

  function loadView(view) {
    let res;
    for (const path in modules) {
      const dir = path.split("views/")[1].split(".vue")[0];
      if (dir === view) {
        res = () => modules[path]();
      }
    }
    return res;
  }
  return {
    GenerateRoutes,
    routes,
    addRoutes,
    defaultRoutes,
    topbarRouters,
    sidebarRouters,
    SET_SIDEBAR_ROUTERS,
  };
});
export function usePermissionStoreHook() {
  return usePermissionStore(store);
}
