import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
// import HomeView from '../views/HomeView.vue'

export const Layout = () => import("@/layout/index.vue");

// 静态路由
export const constantRoutes: RouteRecordRaw[] = [
  {
    path: "/redirect",
    component: Layout,
    meta: { hidden: true },
    children: [
      {
        path: "/redirect/:path(.*)",
        component: () => import("@/views/redirect.vue"),
      },
    ],
  },
  {
    path: "/login",
    component: () => import("@/views/login.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    component: () => import("@/views/error/404.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    component: () => import("@/views/error/401.vue"),
  },
  {
    path: "",
    component: Layout,
    redirect: "index",
    children: [
      {
        path: "index",
        component: () => import("@/views/index.vue"),
        name: "Index",
        meta: {
          title: "首页",
          icon: "dashboard",
          affix: true,
        },
      },
    ],
  },
];

// 动态路由，基于用户权限动态去加载
export const dynamicRoutes = [
  // {
  //   path: "/system/user-auth",
  //   component: Layout,
  //   hidden: true,
  //   permissions: ["system:user:edit"],
  //   children: [
  //     {
  //       path: "role/:userId(\\d+)",
  //       component: () => import("@/views/system/user/authRole.vue"),
  //       name: "AuthRole",
  //       meta: { title: "分配角色", activeMenu: "/system/user" },
  //     },
  //   ],
  // },
  {
    path: "/system/role-auth",
    component: Layout,
    hidden: true,
    permissions: ["system:role:edit"],
    children: [
      {
        path: "user/:orgId(\\d+)/:id(\\d+)/:businessType(\\d+)",
        component: () => import("@/views/system/role/authUser.vue"),
        name: "AuthUser",
        meta: { title: "分配用户", activeMenu: "/system/role" },
      },
    ],
  },
  {
    path: "/system/dict-data",
    component: Layout,
    hidden: true,
    permissions: ["system:dict:list"],
    children: [
      {
        path: "index/:dictId(\\d+)",
        component: () => import("@/views/system/dict/data.vue"),
        name: "Data",
        meta: { title: "字典数据", activeMenu: "/system/dict" },
      },
    ],
  },
  {
    path: "/system/base-dict-data",
    component: Layout,
    hidden: true,
    permissions: ["system:baseData:list"],
    children: [
      {
        path: "index/:baseDictId(\\d+)",
        component: () => import("@/views/system/sysBaseDictType/baseData.vue"),
        name: "baseData",
        meta: { title: "系统基础字典数据", activeMenu: "/system/baseData" },
      },
    ],
  },
  // {
  //   path: '/system/oss-config',
  //   component: Layout,
  //   hidden: true,
  //   permissions: ['system:oss:list'],
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/system/oss/config.vue'),
  //       name: 'OssConfig',
  //       meta: { title: '配置管理', activeMenu: '/system/oss' }
  //     }
  //   ]
  // },
  {
    path: "/tool/gen-edit",
    component: Layout,
    hidden: true,
    permissions: ["tool:gen:edit"],
    children: [
      {
        path: "index/:tableId(\\d+)",
        // component: () => import('@/views/tool/gen/editTable'),
        name: "GenEdit",
        meta: { title: "修改生成配置", activeMenu: "/tool/gen" },
      },
    ],
  },
  // 打印pdf浏览页面
  {
    path: "/print-preview",
    name: "PrintPreview",
    // component: () => import('@/components/PrintPreview/PrintPreview.vue'),
    props: true,
    hidden: true,
    meta: { title: "打印预览" },
    redirect: "",
    children: [],
  },
];

// export const constantRoutes = routes
// 防止连续点击多次路由报错
// let routerPush = Router.prototype.push;
// Router.prototype.push = function push(location) {
//   return routerPush.call(this, location).catch(err => err)
// }

const router = createRouter({
  history: createWebHashHistory(),
  routes: constantRoutes as RouteRecordRaw[],
  // 刷新时，滚动条位置还原
  scrollBehavior: () => ({ left: 0, top: 0 }),
});

export default router;
