import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { setupStore } from "@/store";
// 样式
// import "element-plus/theme-chalk/index.css";
import "@/styles/index.scss";
import "./permission";
import auth from "@/plugins/auth.ts";
// 本地SVG图标
import "virtual:svg-icons-register";

// 国际化
import i18n from "@/lang/index";
// 全局注册自定义指令
import { setupDirective } from "@/directives/index";

// 获取配置
import { getConfigKey, updateConfigByKey } from "@/api/system/config";

import moment from "moment";

import {
  parseTime,
  getWeek,
  handleTree,
  getNormalPath,
  resetForm,
  addDateRange,
  certificateNoInSex,
  selectDictLabel,
  headerCellStyle,
  printPdfPublic,
  strIsUndefined,
  changeFocus,
  addDay,
  getDaysDiff,
  stringToArray,
  certificateNoInAge,
  certificateNoInBirthday,
} from "@/utils/zpPlatform";

const app = createApp(App);
app.use(router);
// 全局注册 状态管理(store)
setupStore(app);
// 全局注册 自定义指令(directive)
setupDirective(app);
// 全局注册
await router.isReady();

// app.use(ElementPlus, {
//   locale: zh,
//   size: "small",
//   zIndex: 3000,
// });
app.use(i18n);

// // 认证对象
app.config.globalProperties.$auth = auth;
// // 缓存对象
// app.config.globalProperties.$cache = cache;
// // 模态框对象
// app.config.globalProperties.$modal = modal;
// // 下载文件
// app.config.globalProperties.$download = download;

// app.config.globalProperties.$baseURL = "http://localhost:9004";

// 定义全局方法
app.config.globalProperties.$parseTime = parseTime;
app.config.globalProperties.$getWeek = getWeek;
app.config.globalProperties.$handleTree = handleTree;
app.config.globalProperties.$getNormalPath = getNormalPath;
app.config.globalProperties.$resetForm = resetForm;
app.config.globalProperties.$getConfigKey = getConfigKey;
app.config.globalProperties.$updateConfigByKey = updateConfigByKey;
app.config.globalProperties.$addDateRange = addDateRange;
app.config.globalProperties.$certificateNoInSex = certificateNoInSex;
app.config.globalProperties.$selectDictLabel = selectDictLabel;
app.config.globalProperties.$moment = moment;
app.config.globalProperties.$headerCellStyle = headerCellStyle;
app.config.globalProperties.$printPdfPublic = printPdfPublic;
app.config.globalProperties.$strIsUndefined = strIsUndefined;
app.config.globalProperties.$changeFocus = changeFocus;
app.config.globalProperties.$addDay = addDay;
app.config.globalProperties.$getDaysDiff = getDaysDiff;
app.config.globalProperties.$stringToArray = stringToArray;
app.config.globalProperties.$certificateNoInAge = certificateNoInAge;
app.config.globalProperties.$certificateNoInBirthday = certificateNoInBirthday;

// 全局clinicId
app.config.globalProperties.$setClinicId = (clinidId: String) => {
  app.config.globalProperties.$clinicId = clinidId;
};

// 全局ClinicInfo
app.config.globalProperties.$setClinicInfo = (clinicInfo: {}) => {
  app.config.globalProperties.$clinicInfo = clinicInfo;
};
// 全局patientId
app.config.globalProperties.$setPatientId = (patientId: String) => {
  app.config.globalProperties.$patientId = patientId;
};
// 门诊、住院属性 门诊 ：4 ；住院 ：1
app.config.globalProperties.$setInoroutFlag = (inoroutFlag: String) => {
  app.config.globalProperties.$inoroutFlag = inoroutFlag;
};

// 定义系统公共字符串
app.config.globalProperties.$OrgIdIsEmpty = "请先选择机构后，再进行后续操作";

app.mount("#app");
