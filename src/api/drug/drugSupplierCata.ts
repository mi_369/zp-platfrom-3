import request from '@/utils/request'

// 查询药品供应商生产商维护列表 (分页)
export function selectPageDrugSupplierCata(query) {
    return request({
        url: '/drug/drugSupplierCata/selectPage',
        method: 'get',
        params: query
    })
}

// 查询药品供应商生产商维护列表 (不分页)
export function listDrugSupplierCata(query) {
    return request({
        url: '/drug/drugSupplierCata/list',
        method: 'get',
        params: query
    })
}

// 查询药品供应商生产商维护详细
export function getDrugSupplierCata(id) {
    return request({
        url: '/drug/drugSupplierCata/' + id,
        method: 'get'
    })
}

// 新增药品供应商生产商维护
export function addDrugSupplierCata(data) {
    return request({
        url: '/drug/drugSupplierCata',
        method: 'post',
        data: data
    })
}

// 修改药品供应商生产商维护
export function updateDrugSupplierCata(data) {
    return request({
        url: '/drug/drugSupplierCata',
        method: 'put',
        data: data
    })
}

// 删除药品供应商生产商维护
export function delDrugSupplierCata(id) {
    return request({
        url: '/drug/drugSupplierCata/' + id,
        method: 'delete'
    })
}

/**
 * 刷新缓存
 * @param deptId
 * @returns {*}
 */
export function refreshCacheDrugSupplierCata() {
    return request({
        url: '/drug/drugSupplierCata/refreshCacheDrugSupplierCata',
        method: 'get'
    })
}
