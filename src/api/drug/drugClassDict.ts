import request from '@/utils/request'

// 查询药品功效类别字典列表 (分页)
export function selectPageDrugClassDict(query) {
    return request({
        url: '/drug/drugClassDict/selectPage',
        method: 'get',
        params: query
    })
}

// 查询药品功效类别字典列表 (不分页)
export function listDrugClassDict(query) {
    return request({
        url: '/drug/drugClassDict/list',
        method: 'get',
        params: query
    })
}

// 查询药品功效类别字典详细
export function getDrugClassDict(id) {
    return request({
        url: '/drug/drugClassDict/' + id,
        method: 'get'
    })
}

// 新增药品功效类别字典
export function addDrugClassDict(data) {
    return request({
        url: '/drug/drugClassDict',
        method: 'post',
        data: data
    })
}

// 修改药品功效类别字典
export function updateDrugClassDict(data) {
    return request({
        url: '/drug/drugClassDict',
        method: 'put',
        data: data
    })
}

// 删除药品功效类别字典
export function delDrugClassDict(id) {
    return request({
        url: '/drug/drugClassDict/' + id,
        method: 'delete'
    })
}
