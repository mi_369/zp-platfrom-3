import request from '@/utils/request'

// 查询处方权限配置列表 (分页)
export function selectPageDrugPresPermissionsConf(query) {
    return request({
        url: '/drug/drugPresPermissionsConf/selectPage',
        method: 'get',
        params: query
    })
}

// 查询处方权限配置列表 (不分页)
export function listDrugPresPermissionsConf(query) {
    return request({
        url: '/drug/drugPresPermissionsConf/list',
        method: 'get',
        params: query
    })
}

// 查询处方权限配置详细
export function getDrugPresPermissionsConf(id) {
    return request({
        url: '/drug/drugPresPermissionsConf/' + id,
        method: 'get'
    })
}

// 新增处方权限配置
export function addDrugPresPermissionsConf(data) {
    return request({
        url: '/drug/drugPresPermissionsConf',
        method: 'post',
        data: data
    })
}

// 修改处方权限配置
export function updateDrugPresPermissionsConf(data) {
    return request({
        url: '/drug/drugPresPermissionsConf',
        method: 'put',
        data: data
    })
}

// 删除处方权限配置
export function delDrugPresPermissionsConf(id) {
    return request({
        url: '/drug/drugPresPermissionsConf/' + id,
        method: 'delete'
    })
}
// 同步机构人员
export function syncOrgPerson (orgId) {
    return request({
        url: '/drug/drugPresPermissionsConf/syncOrgPerson/' + orgId,
        method: 'put'
    })
}
