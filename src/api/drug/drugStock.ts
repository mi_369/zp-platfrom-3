import request from '@/utils/request'

// 查询药品库存列表 (分页)
export function selectPageStock(query) {
    return request({
        url: '/drug/stock/selectPage',
        method: 'get',
        params: query
    })
}

// 查询药品库存列表 (不分页)
export function listStock(query) {
    return request({
        url: '/drug/stock/list',
        method: 'get',
        params: query
    })
}

// 查询药品库存详细
export function getStock(id) {
    return request({
        url: '/drug/stock/' + id,
        method: 'get'
    })
}

// 新增药品库存
export function addStock(data) {
    return request({
        url: '/drug/stock',
        method: 'post',
        data: data
    })
}

// 修改药品库存
export function updateStock(data) {
    return request({
        url: '/drug/stock',
        method: 'put',
        data: data
    })
}

// 删除药品库存
export function delStock(id) {
    return request({
        url: '/drug/stock/' + id,
        method: 'delete'
    })
}
