import request from '@/utils/request'

// 查询库房单位与科室对照列表 (分页)
export function selectPageStorageVsDept(query) {
    return request({
        url: '/drug/storageVsDept/selectPage',
        method: 'get',
        params: query
    })
}

// 查询库房单位与科室对照列表 (不分页)
export function listStorageVsDept(query) {
    return request({
        url: '/drug/storageVsDept/list',
        method: 'get',
        params: query
    })
}

// 查询库房单位与科室对照详细
export function getStorageVsDept(id) {
    return request({
        url: '/drug/storageVsDept/' + id,
        method: 'get'
    })
}

// 新增库房单位与科室对照
export function addStorageVsDept(data) {
    return request({
        url: '/drug/storageVsDept',
        method: 'post',
        data: data
    })
}

// 修改库房单位与科室对照
export function updateDept(data) {
    return request({
        url: '/drug/storageVsDept',
        method: 'put',
        data: data
    })
}

// 删除库房单位与科室对照
export function delDept(id) {
    return request({
        url: '/drug/storageVsDept/' + id,
        method: 'delete'
    })
}
