import request from "@/utils/request";

// 查询药品字典基本信息列表 (分页)
export function selectPageDrugInfo(query) {
  return request({
    url: "/drug/drugInfo/selectPage",
    method: "get",
    params: query,
  });
}

// 查询药品字典基本信息列表 (不分页)
export function listDrugInfo(query) {
  return request({
    url: "/drug/drugInfo/list",
    method: "get",
    params: query,
  });
}

// 查询药品字典基本信息详细
export function getDrugInfo(id) {
  return request({
    url: "/drug/drugInfo/" + id,
    method: "get",
  });
}

// 新增药品字典基本信息
export function addDrugInfo(data) {
  return request({
    url: "/drug/drugInfo",
    method: "post",
    data: data,
  });
}

// 修改药品字典基本信息
export function updateDrugInfo(data) {
  return request({
    url: "/drug/drugInfo",
    method: "put",
    data: data,
  });
}

// 删除药品字典基本信息
export function delDrugInfo(id) {
  return request({
    url: "/drug/drugInfo/" + id,
    method: "delete",
  });
}
