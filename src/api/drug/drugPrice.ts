import request from '@/utils/request'

// 查询药品价表列表 (分页)
export function selectPagePrice(query) {
    return request({
        url: '/drug/price/selectPage',
        method: 'get',
        params: query
    })
}

// 查询药品价表列表 (不分页)
export function listPrice(query) {
    return request({
        url: '/drug/price/list',
        method: 'get',
        params: query
    })
}

// 查询药品价表详细
export function getPrice(id) {
    return request({
        url: '/drug/price/' + id,
        method: 'get'
    })
}

// 新增药品价表
export function addPrice(data) {
    return request({
        url: '/drug/price',
        method: 'post',
        data: data
    })
}

// 修改药品价表
export function updatePrice(data) {
    return request({
        url: '/drug/price',
        method: 'put',
        data: data
    })
}

// 停止或恢复供应
export function supplySignStopOrRestore(data) {
    return request({
        url: '/drug/price/supplySignStopOrRestore',
        method: 'post',
        data: data
    })
}

// 价表停用或启用
export function stopPrice(data) {
    return request({
        url: '/drug/price/stopPrice',
        method: 'post',
        data: data
    })
}

// 删除药品价表
export function delPrice(id) {
    return request({
        url: '/drug/price/' + id,
        method: 'delete'
    })
}
