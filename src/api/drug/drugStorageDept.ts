import request from '@/utils/request'

// 查询药品库存单位字典列表 (分页)
export function selectPageDrugStorageDept(query) {
    return request({
        url: '/drug/drugStorageDept/selectPage',
        method: 'get',
        params: query
    })
}

// 查询药品库存单位字典列表 (不分页)
export function listDrugStorageDept(query) {
    return request({
        url: '/drug/drugStorageDept/list',
        method: 'get',
        params: query
    })
}

// 查询药品库存单位字典详细
export function getDrugStorageDept(id) {
    return request({
        url: '/drug/drugStorageDept/' + id,
        method: 'get'
    })
}

// 新增药品库存单位字典
export function addDrugStorageDept(data) {
    return request({
        url: '/drug/drugStorageDept',
        method: 'post',
        data: data
    })
}

// 修改药品库存单位字典
export function updateDrugStorageDept(data) {
    return request({
        url: '/drug/drugStorageDept',
        method: 'put',
        data: data
    })
}

// 删除药品库存单位字典
export function delDrugStorageDept(id) {
    return request({
        url: '/drug/drugStorageDept/' + id,
        method: 'delete'
    })
}

// 刷新缓存（药品库存单位字典）
export function refreshCacheDrugStorageDept() {
    return request({
        url: '/drug/drugStorageDept/refreshCacheDrugStorageDept',
        method: 'get'
    })
}
