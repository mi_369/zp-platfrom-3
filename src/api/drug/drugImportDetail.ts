import request from '@/utils/request'

// 查询药品入库明细列表 (分页)
export function selectPageDrugImportDetail(query) {
    return request({
        url: '/drug/drugImportDetail/selectPage',
        method: 'get',
        params: query
    })
}

// 查询药品入库明细列表 (不分页)
export function listDrugImportDetail(query) {
    return request({
        url: '/drug/drugImportDetail/list',
        method: 'get',
        params: query
    })
}

// 查询药品入库明细详细
export function getDrugImportDetail(id) {
    return request({
        url: '/drug/drugImportDetail/' + id,
        method: 'get'
    })
}

// 新增药品入库明细
export function addDrugImportDetail(data) {
    return request({
        url: '/drug/drugImportDetail',
        method: 'post',
        data: data
    })
}

// 修改药品入库明细
export function updateDrugImportDetail(data) {
    return request({
        url: '/drug/drugImportDetail',
        method: 'put',
        data: data
    })
}

// 删除药品入库明细
export function delDrugImportDetail(id) {
    return request({
        url: '/drug/drugImportDetail/' + id,
        method: 'delete'
    })
}
