import request from '@/utils/request'

// 查询药品入库主表列表 (分页)
export function selectPageDrugImportMaster(query) {
    return request({
        url: '/drug/drugImportMaster/selectPage',
        method: 'get',
        params: query
    })
}

// 查询药品入库主表列表 (不分页)
export function listDrugImportMaster(query) {
    return request({
        url: '/drug/drugImportMaster/list',
        method: 'get',
        params: query
    })
}

// 入库药品检索
export function getAutoDrugPriceInfo(query) {
    return request({
        url: '/drug/drugImportMaster/getAutoDrugPriceInfo',
        method: 'get',
        params: query
    })
}

// 查询药品入库主表详细
export function getDrugImportMaster(id) {
    return request({
        url: '/drug/drugImportMaster/' + id,
        method: 'get'
    })
}

// 新增药品入库主表
export function addDrugImportMaster(data) {
    return request({
        url: '/drug/drugImportMaster',
        method: 'post',
        data: data
    })
}

// 修改药品入库主表
export function updateDrugImportMaster(data) {
    return request({
        url: '/drug/drugImportMaster',
        method: 'put',
        data: data
    })
}

// 删除药品入库主表
export function delDrugImportMaster(id) {
    return request({
        url: '/drug/drugImportMaster/' + id,
        method: 'delete'
    })
}
