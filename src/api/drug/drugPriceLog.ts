import request from '@/utils/request'

// 查询base列表 (分页)
export function selectPageDrugPriceLog(query) {
    return request({
        url: '/drug/drugPriceLog/selectPage',
        method: 'get',
        params: query
    })
}

// 查询base列表 (不分页)
export function listDrugPriceLog(query) {
    return request({
        url: '/drug/drugPriceLog/list',
        method: 'get',
        params: query
    })
}

// 查询base详细
export function getDrugPriceLog(id) {
    return request({
        url: '/drug/drugPriceLog/' + id,
        method: 'get'
    })
}

// 新增base
export function addDrugPriceLog(data) {
    return request({
        url: '/drug/drugPriceLog',
        method: 'post',
        data: data
    })
}

// 修改base
export function updateDrugPricelog(data) {
    return request({
        url: '/drug/DrugPricelog',
        method: 'put',
        data: data
    })
}

// 删除base
export function delDrugPricelog(id) {
    return request({
        url: '/drug/DrugPricelog/' + id,
        method: 'delete'
    })
}
