import request from '@/utils/request'

// 查询出入库分类字典表列表 (分页)
export function selectPageDrugInoutClass(query) {
    return request({
        url: '/drug/drugInoutClass/selectPage',
        method: 'get',
        params: query
    })
}

// 查询出入库分类字典表列表 (不分页)
export function listDrugInoutClass(query) {
    return request({
        url: '/drug/drugInoutClass/list',
        method: 'get',
        params: query
    })
}

// 根据科室id 出入库类型获取库房对应的出入库方式
export function getStoreroomDistinguish(query) {
    return request({
        url: '/drug/drugInoutClass/getStoreroomDistinguish',
        method: 'get',
        params: query
    })
}

// 查询出入库分类字典表详细
export function getDrugInoutClass(id) {
    return request({
        url: '/drug/drugInoutClass/' + id,
        method: 'get'
    })
}

// 新增出入库分类字典表
export function addDrugInoutClass(data) {
    return request({
        url: '/drug/drugInoutClass',
        method: 'post',
        data: data
    })
}

// 修改出入库分类字典表
export function updateDrugInoutClass(data) {
    return request({
        url: '/drug/drugInoutClass',
        method: 'put',
        data: data
    })
}

// 删除出入库分类字典表
export function delDrugInoutClass(id) {
    return request({
        url: '/drug/drugInoutClass/' + id,
        method: 'delete'
    })
}
