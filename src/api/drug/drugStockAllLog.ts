import request from '@/utils/request'

// 查询药品库存流转记录列表 (分页)
export function selectPageDrugStockAllLog(query) {
    return request({
        url: '/drug/drugStockAllLog/selectPage',
        method: 'get',
        params: query
    })
}

// 查询药品库存流转记录列表 (不分页)
export function listDrugStockAllLog(query) {
    return request({
        url: '/drug/drugStockAllLog/list',
        method: 'get',
        params: query
    })
}

// 查询药品库存流转记录详细
export function getDrugStockAllLog(id) {
    return request({
        url: '/drug/drugStockAllLog/' + id,
        method: 'get'
    })
}

// 新增药品库存流转记录
export function addDrugStockAllLog(data) {
    return request({
        url: '/drug/drugStockAllLog',
        method: 'post',
        data: data
    })
}

// 修改药品库存流转记录
export function updateDrugStockAllLog(data) {
    return request({
        url: '/drug/drugStockAllLog',
        method: 'put',
        data: data
    })
}

// 删除药品库存流转记录
export function delDrugStockAllLog(id) {
    return request({
        url: '/drug/drugStockAllLog/' + id,
        method: 'delete'
    })
}
