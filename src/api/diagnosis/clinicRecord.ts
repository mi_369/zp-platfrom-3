import request from "@/utils/request";

// 查询患者病历表列表 (分页)
export function selectPageClinicRecord(query) {
  return request({
    url: "/business/clinicRecord/selectPage",
    method: "get",
    params: query,
  });
}

// 查询患者病历表列表 (不分页)
export function listClinicRecord(query) {
  return request({
    url: "/business/clinicRecord/list",
    method: "get",
    params: query,
  });
}

// 查询患者病历表详细
export function getClinicRecord(id) {
  return request({
    url: "/business/clinicRecord/" + id,
    method: "get",
  });
}

// 新增患者病历表
export function addClinicRecord(data) {
  return request({
    url: "/business/clinicRecord",
    method: "post",
    data: data,
  });
}

// 修改患者病历表
export function updateClinicRecord(data) {
  return request({
    url: "/business/clinicRecord",
    method: "put",
    data: data,
  });
}

// 删除患者病历表
export function delClinicRecord(id) {
  return request({
    url: "/business/clinicRecord/" + id,
    method: "delete",
  });
}
