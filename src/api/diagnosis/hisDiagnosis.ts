import request from "@/utils/request";

// 查询患者诊断表列表 (分页)
export function selectPageHisDiagnosis(query) {
  return request({
    url: "/business/hisDiagnosis/selectPage",
    method: "get",
    params: query,
  });
}

// 查询患者诊断表列表 (不分页)
export function listHisDiagnosis(query) {
  return request({
    url: "/business/hisDiagnosis/list",
    method: "get",
    params: query,
  });
}

// 查询患者诊断表详细
export function getHisDiagnosis(id) {
  return request({
    url: "/business/hisDiagnosis/" + id,
    method: "get",
  });
}

// 新增患者诊断表
export function addHisDiagnosis(data) {
  return request({
    url: "/business/hisDiagnosis",
    method: "post",
    data: data,
  });
}

// 修改患者诊断表
export function updateHisDiagnosis(data) {
  return request({
    url: "/business/hisDiagnosis",
    method: "put",
    data: data,
  });
}

// 删除患者诊断表
export function delHisDiagnosis(id) {
  return request({
    url: "/business/hisDiagnosis/" + id,
    method: "delete",
  });
}
