import request from "@/utils/request";

// 查询自定义服务信息列表（分页）
export function listGroup(query) {
  return request({
    url: "/system/group/list",
    method: "get",
    params: query,
  });
}

// 查询自定义服务信息列表（不分页）
export function selectList(query) {
  return request({
    url: "/system/group/selectList",
    method: "get",
    params: query,
  });
}

// 查询自定义服务信息详细
export function getGroup(roleGroupId) {
  return request({
    url: "/system/group/" + roleGroupId,
    method: "get",
  });
}

// 新增自定义服务信息
export function addGroup(data) {
  return request({
    url: "/system/group",
    method: "post",
    data: data,
  });
}

// 修改自定义服务信息
export function updateGroup(data) {
  return request({
    url: "/system/group",
    method: "put",
    data: data,
  });
}

// 删除自定义服务信息
export function delGroup(roleGroupId) {
  return request({
    url: "/system/group/" + roleGroupId,
    method: "delete",
  });
}

// 查询角色修改、新增页面自定义服务列
export function getNotExistsRoleGroup(data) {
  return request({
    url: "/system/group/getNotExistsRoleGroup",
    method: "get",
    params: data,
  });
}

// 根据userId获取已选择的自定义服务列表
export function selectListChooseRoleGroup(data) {
  return request({
    url: "/system/group/selectListChooseRoleGroup",
    method: "get",
    params: data,
  });
}
