import request from "@/utils/request";

// 查询角色与已选择自定义服务对照列表
export function listChooseRoleGroup(query) {
  return request({
    url: "/system/chooseRoleGroup/list",
    method: "get",
    params: query,
  });
}

// 查询角色与已选择自定义服务对照详细
export function getChooseRoleGroup(roleId) {
  return request({
    url: "/system/chooseRoleGroup/" + roleId,
    method: "get",
  });
}

// 新增角色与已选择自定义服务对照
export function addChooseRoleGroup(data) {
  return request({
    url: "/system/chooseRoleGroup",
    method: "post",
    data: data,
  });
}

// 修改角色与已选择自定义服务对照
export function updateChooseRoleGroup(data) {
  return request({
    url: "/system/chooseRoleGroup",
    method: "put",
    data: data,
  });
}

// 删除角色与已选择自定义服务对照
export function delChooseRoleGroup(data) {
  return request({
    url: "/system/chooseRoleGroup/delChooseRoleGroup",
    method: "post",
    data: data,
  });
}
