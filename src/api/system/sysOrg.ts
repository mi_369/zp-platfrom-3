import request from "@/utils/request";

// 查询机构列表 (分页)
export function selectPageSysOrg(query: any) {
  return request({
    url: "/system/SysOrg/selectPage",
    method: "get",
    params: query,
  });
}

// 查询机构列表 (不分页)
export function listSysOrg(query: any) {
  return request({
    url: "/system/SysOrg/list",
    method: "get",
    params: query,
  });
}

// 查询机构详细
export function getSysOrg(orgId: string) {
  return request({
    url: "/system/SysOrg/" + orgId,
    method: "get",
  });
}

// 新增机构
export function addSysOrg(data: any) {
  return request({
    url: "/system/SysOrg",
    method: "post",
    data: data,
  });
}

// 修改机构
export function updateSysOrg(data: any) {
  return request({
    url: "/system/SysOrg",
    method: "put",
    data: data,
  });
}

// 删除机构
export function delSysOrg(orgId: string) {
  return request({
    url: "/system/SysOrg/" + orgId,
    method: "delete",
  });
}

// 查询机构下拉树结构
export function treeSelect(query: any) {
  return request({
    url: "/system/SysOrg/treeSelect",
    params: query,
    method: "get",
  });
}

// 机构状态修改
export function changeOrgStatus(orgId: any, status: any) {
  const data = {
    orgId,
    status,
  };
  return request({
    url: "/system/SysOrg/changeStatus",
    method: "put",
    data: data,
  });
}
