import request from "@/utils/request";

// 查询菜单列表
export function listMenu(query) {
  return request({
    url: "/system/menu/list",
    method: "get",
    params: query,
  });
}

// 查询菜单详细
export function getMenu(menuId) {
  return request({
    url: "/system/menu/" + menuId,
    method: "get",
  });
}

// 查询菜单下拉树结构
export function treeselect() {
  return request({
    url: "/system/menu/treeselect",
    method: "get",
  });
}

// 根据角色ID查询菜单下拉树结构
export function roleMenuTreeselect(roleId) {
  return request({
    url: "/system/menu/roleMenuTreeSelect/" + roleId,
    method: "get",
  });
}

// 根据自定义服务ID查询菜单下拉树结构
export function selectMenuListByRoleGroupId(roleGroupId) {
  return request({
    url: "/system/menu/selectMenuListByRoleGroupId/" + roleGroupId,
    method: "get",
  });
}

// 新增菜单
export function addMenu(data) {
  return request({
    url: "/system/menu",
    method: "post",
    data: data,
  });
}

// 修改菜单
export function updateMenu(data) {
  return request({
    url: "/system/menu",
    method: "put",
    data: data,
  });
}

// 删除菜单
export function delMenu(menuId) {
  return request({
    url: "/system/menu/" + menuId,
    method: "delete",
  });
}

// 加载已选择角色的权限菜单
export function selectChooseRoleGroupMenu(data) {
  return request({
    url: "/system/menu/selectChooseRoleGroupMenu",
    method: "post",
    data: data,
  });
}

// 获取路由
// export const getRouters = (data) => {
export const getRouters = () => {
  return request({
    url: "/system/menu/getRouters",
    method: "get",
    params: null,
  });
};

/**
 * 通过用户id获取用户所有菜单list
 * @param data
 * @returns {AxiosPromise}
 */
export function getUserRouters(data) {
  return request({
    url: "/system/menu/getUserRouters",
    method: "get",
    params: data,
  });
}
