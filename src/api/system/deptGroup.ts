import request from "@/utils/request";

// 查询科室分组列表 (分页)
export function selectPageDeptGroup(query) {
  return request({
    url: "/system/deptGroup/selectPage",
    method: "get",
    params: query,
  });
}

// 查询科室分组列表 (不分页)
export function listDeptGroup(query) {
  return request({
    url: "/system/deptGroup/list",
    method: "get",
    params: query,
  });
}

// 查询科室分组详细
export function getDeptGroup(deptGroupId) {
  return request({
    url: "/system/deptGroup/" + deptGroupId,
    method: "get",
  });
}

// 新增科室分组
export function addDeptGroup(data) {
  return request({
    url: "/system/deptGroup",
    method: "post",
    data: data,
  });
}

// 修改科室分组
export function updateDeptGroup(data) {
  return request({
    url: "/system/deptGroup",
    method: "put",
    data: data,
  });
}

// 删除科室分组
export function delDeptGroup(deptGroupId) {
  return request({
    url: "/system/deptGroup/" + deptGroupId,
    method: "delete",
  });
}
