import request from "@/utils/request";

// 查询系统平台字典列表 (分页)
export function selectPageBaseData(query) {
  return request({
    url: "/system/baseData/selectPage",
    method: "get",
    params: query,
  });
}

// 查询系统平台字典列表 (不分页)
export function listBaseData(query) {
  return request({
    url: "/system/baseData/list",
    method: "get",
    params: query,
  });
}

// 查询系统平台字典详细
export function getBaseData(id) {
  return request({
    url: "/system/baseData/" + id,
    method: "get",
  });
}

// 新增系统平台字典
export function addBaseData(data) {
  return request({
    url: "/system/baseData",
    method: "post",
    data: data,
  });
}

// 修改系统平台字典
export function updateBaseData(data) {
  return request({
    url: "/system/baseData",
    method: "put",
    data: data,
  });
}

// 删除系统平台字典
export function delBaseData(id) {
  return request({
    url: "/system/baseData/" + id,
    method: "delete",
  });
}
