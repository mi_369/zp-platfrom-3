import request from "@/utils/request";

// 查询菜单下拉树结构
export function roleGroupMenuTree(roleGroupId) {
  return request({
    url: "/system/groupMenu/roleGroupMenuTree/" + roleGroupId,
    method: "post",
  });
}
