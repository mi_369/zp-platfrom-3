import request from '@/utils/request'

// 查询字典类型列表 (分页)
export function selectPageSysBaseDictType (query) {
    return request({
        url: '/system/sysBaseDictType/selectPage',
        method: 'get',
        params: query
    })
}

// 查询字典类型列表 (不分页)
export function listSysBaseDictType (query) {
    return request({
        url: '/system/sysBaseDictType/list',
        method: 'get',
        params: query
    })
}

// 查询字典类型详细
export function getSysBaseDictType (baseDictId) {
    return request({
        url: '/system/sysBaseDictType/' + baseDictId,
        method: 'get'
    })
}

// 新增字典类型
export function addSysBaseDictType (data) {
    return request({
        url: '/system/sysBaseDictType',
        method: 'post',
        data: data
    })
}

// 修改字典类型
export function updateSysBaseDictType (data) {
    return request({
        url: '/system/sysBaseDictType',
        method: 'put',
        data: data
    })
}

// 删除字典类型
export function delSysBaseDictType (baseDictId) {
    return request({
        url: '/system/sysBaseDictType/' + baseDictId,
        method: 'delete'
    })
}
