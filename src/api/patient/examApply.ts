import request from '@/utils/request'

// 查询检查申请主列表 (分页)
export function selectPageExamApply(query) {
    return request({
        url: '/business/examApply/selectPage',
        method: 'get',
        params: query
    })
}

// 查询检查申请主列表 (不分页)
export function listExamApply(query) {
    return request({
        url: '/business/examApply/list',
        method: 'get',
        params: query
    })
}

// 查询检查申请主详细
export function getExamApply(id) {
    return request({
        url: '/business/examApply/' + id,
        method: 'get'
    })
}

// 新增检查申请主
export function addExamApply(data) {
    return request({
        url: '/business/examApply',
        method: 'post',
        data: data
    })
}

// 修改检查申请主
export function updateExamApply(data) {
    return request({
        url: '/business/examApply',
        method: 'put',
        data: data
    })
}

// 删除检查申请主
export function delExamApply(id) {
    return request({
        url: '/business/examApply/' + id,
        method: 'delete'
    })
}

// 保存检查申请明显
export function saveExamApplyAndItem(data) {
    return request({
        url: '/business/examApply/saveExamApplyAndItem',
        method: 'post',
        data: data
    })
}

