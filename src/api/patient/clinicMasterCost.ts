import request from "@/utils/request";

// 查询门诊挂号费用明细列表 (分页)
export function selectPageClinicMasterCost(query) {
  return request({
    url: "/finance/clinicMasterCost/selectPage",
    method: "get",
    params: query,
  });
}

// 查询门诊挂号费用明细列表 (不分页)
export function listClinicMasterCost(query) {
  return request({
    url: "/finance/clinicMasterCost/list",
    method: "get",
    params: query,
  });
}

// 查询门诊挂号费用明细详细
export function getClinicMasterCost(id) {
  return request({
    url: "/finance/clinicMasterCost/" + id,
    method: "get",
  });
}

// 新增门诊挂号费用明细
export function addClinicMasterCost(data) {
  return request({
    url: "/finance/clinicMasterCost",
    method: "post",
    data: data,
  });
}

// 修改门诊挂号费用明细
export function updateClinicMasterCost(data) {
  return request({
    url: "/finance/clinicMasterCost",
    method: "put",
    data: data,
  });
}

// 删除门诊挂号费用明细
export function delClinicMasterCost(id) {
  return request({
    url: "/finance/clinicMasterCost/" + id,
    method: "delete",
  });
}
