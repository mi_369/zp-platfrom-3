import request from '@/utils/request'

// 查询检查申请子列表 (分页)
export function selectPageExamItem(query) {
    return request({
        url: '/business/examItem/selectPage',
        method: 'get',
        params: query
    })
}

// 查询检查申请子列表 (不分页)
export function listExamItem(query) {
    return request({
        url: '/business/examItem/list',
        method: 'get',
        params: query
    })
}

// 查询检查申请子详细
export function getExamItem(id) {
    return request({
        url: '/business/examItem/' + id,
        method: 'get'
    })
}

// 新增检查申请子
export function addExamItem(data) {
    return request({
        url: '/business/examItem',
        method: 'post',
        data: data
    })
}

// 修改检查申请子
export function updateExamItem(data) {
    return request({
        url: '/business/examItem',
        method: 'put',
        data: data
    })
}

// 删除检查申请子
export function delExamItem(id) {
    return request({
        url: '/business/examItem/' + id,
        method: 'delete'
    })
}
