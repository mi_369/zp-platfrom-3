import request from "@/utils/request";

// 查询门诊就诊记录列表 (分页)
export function selectPageClinicMaster(query) {
  return request({
    url: "/business/clinicMaster/selectPage",
    method: "get",
    params: query,
  });
}

// 查询门诊就诊记录列表 (不分页)
export function listClinicMaster(query) {
  return request({
    url: "/business/clinicMaster/list",
    method: "get",
    params: query,
  });
}

export function getPatientCount (query) {
  return request({
    url: "/business/clinicMaster/getPatientCount",
    method: "get",
    params: query,
  });
}

export function getNumGroupClinicType (query) {
  return request({
    url: "/business/clinicMaster/getNumGroupClinicType",
    method: "get",
    params: query,
  });
}

// 查询门诊就诊记录详细
export function getClinicMaster(id) {
  return request({
    url: "/business/clinicMaster/" + id,
    method: "get",
  });
}

// 新增门诊就诊记录
export function addClinicMaster(data) {
  return request({
    url: "/business/clinicMaster",
    method: "post",
    data: data,
  });
}

// 修改门诊就诊记录
export function updateClinicMaster(data) {
  return request({
    url: "/business/clinicMaster",
    method: "put",
    data: data,
  });
}

// 删除门诊就诊记录
export function delClinicMaster(id) {
  return request({
    url: "/business/clinicMaster/" + id,
    method: "delete",
  });
}
