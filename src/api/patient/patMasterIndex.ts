import request from '@/utils/request'

// 查询病人信息表列表 (分页)
export function selectPagePatMasterIndex(query) {
    return request({
        url: '/patient/patMasterIndex/selectPage',
        method: 'get',
        params: query
    })
}

// 查询病人信息表列表 (不分页)
export function listPatMasterIndex(query) {
    return request({
        url: '/patient/patMasterIndex/list',
        method: 'get',
        params: query
    })
}

// 查询病人信息表详细
export function getPatMasterIndex(id) {
    return request({
        url: '/patient/patMasterIndex/' + id,
        method: 'get'
    })
}

// 新增病人信息表
export function addPatMasterIndex(data) {
    return request({
        url: '/patient/patMasterIndex',
        method: 'post',
        data: data
    })
}

// 修改病人信息表
export function updatePatMasterIndex(data) {
    return request({
        url: '/patient/patMasterIndex',
        method: 'put',
        data: data
    })
}

// 删除病人信息表
export function delPatMasterIndex(id) {
    return request({
        url: '/patient/patMasterIndex/' + id,
        method: 'delete'
    })
}
