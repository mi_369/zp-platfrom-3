import request from '@/utils/request'

// 查询处置治疗列表 (分页)
export function selectPageOutpTreatRec(query) {
    return request({
        url: '/business/outpTreatRec/selectPage',
        method: 'get',
        params: query
    })
}

// 查询处置治疗列表 (不分页)
export function listOutpTreatRec(query) {
    return request({
        url: '/business/outpTreatRec/list',
        method: 'get',
        params: query
    })
}

// 查询处置治疗详细
export function getOutpTreatRec(id) {
    return request({
        url: '/business/outpTreatRec/' + id,
        method: 'get'
    })
}

// 新增处置治疗
export function addOutpTreatRec(data) {
    return request({
        url: '/business/outpTreatRec',
        method: 'post',
        data: data
    })
}

// 修改处置治疗
export function updateOutpTreatRec(data) {
    return request({
        url: '/business/outpTreatRec',
        method: 'put',
        data: data
    })
}

// 删除处置治疗
export function delOutpTreatRec(id) {
    return request({
        url: '/business/outpTreatRec/' + id,
        method: 'delete'
    })
}
