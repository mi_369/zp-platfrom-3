import request from '@/utils/request'

// 查询门诊项目明细列表 (分页)
export function selectPageClinicOrdersCost(query) {
    return request({
        url: '/business/clinicOrdersCost/selectPage',
        method: 'get',
        params: query
    })
}

// 查询门诊项目明细列表 (不分页)
export function listClinicOrdersCost(query) {
    return request({
        url: '/business/clinicOrdersCost/list',
        method: 'get',
        params: query
    })
}

// 查询门诊项目明细详细
export function getClinicOrdersCost(id) {
    return request({
        url: '/business/clinicOrdersCost/' + id,
        method: 'get'
    })
}

// 新增门诊项目明细
export function addClinicOrdersCost(data) {
    return request({
        url: '/business/clinicOrdersCost',
        method: 'post',
        data: data
    })
}

// 修改门诊项目明细
export function updateClinicOrdersCost(data) {
    return request({
        url: '/business/clinicOrdersCost',
        method: 'put',
        data: data
    })
}

// 删除门诊项目明细
export function delClinicOrdersCost(id) {
    return request({
        url: '/business/clinicOrdersCost/' + id,
        method: 'delete'
    })
}
