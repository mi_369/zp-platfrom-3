import request from '@/utils/request'

// 查询门诊医嘱列表 (分页)
export function selectPageClinicOrders(query) {
    return request({
        url: '/business/clinicOrders/selectPage',
        method: 'get',
        params: query
    })
}

// 查询门诊医嘱列表 (不分页)
export function listClinicOrders(query) {
    return request({
        url: '/business/clinicOrders/list',
        method: 'get',
        params: query
    })
}

// 查询门诊医嘱详细
export function getClinicOrders(id) {
    return request({
        url: '/business/clinicOrders/' + id,
        method: 'get'
    })
}

// 新增门诊医嘱
export function addClinicOrders(data) {
    return request({
        url: '/business/clinicOrders',
        method: 'post',
        data: data
    })
}

// 修改门诊医嘱
export function updateClinicOrders(data) {
    return request({
        url: '/business/clinicOrders',
        method: 'put',
        data: data
    })
}

// 删除门诊医嘱
export function delClinicOrders(id) {
    return request({
        url: '/business/clinicOrders/' + id,
        method: 'delete'
    })
}
