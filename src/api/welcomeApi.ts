import request from "@/utils/request";

/**
 * 保存用户快捷方式菜单
 * @param data
 * @returns {AxiosPromise}
 */
export function shortcutNavigation(data) {
  return request({
    url: "/system/menu/setShortcutNavigation",
    method: "post",
    data: data,
  });
}

/**
 * 获取用户快捷方式菜单
 * @param userId
 * @returns {AxiosPromise}
 */
export function getShortcutNavigation(userId) {
  return request({
    url: "/system/menu/getShortcutNavigation/" + userId,
    method: "get",
  });
}
