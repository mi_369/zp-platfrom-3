import request from "@/utils/request";

// 查询挂号收费类型表列表 (分页)
export function selectPageRegistPayType(query) {
  return request({
    url: "/finance/registPayType/selectPage",
    method: "get",
    params: query,
  });
}

// 查询挂号收费类型表列表 (不分页)
export function listRegistPayType(query) {
  return request({
    url: "/finance/registPayType/list",
    method: "get",
    params: query,
  });
}

// 查询挂号收费类型表详细
export function getRegistPayType(id) {
  return request({
    url: "/finance/registPayType/" + id,
    method: "get",
  });
}

// 新增挂号收费类型表
export function addRegistPayType(data) {
  return request({
    url: "/finance/registPayType",
    method: "post",
    data: data,
  });
}

// 修改挂号收费类型表
export function updateRegistPayType(data) {
  return request({
    url: "/finance/registPayType",
    method: "put",
    data: data,
  });
}

// 删除挂号收费类型表
export function delRegistPayType(id) {
  return request({
    url: "/finance/registPayType/" + id,
    method: "delete",
  });
}

// 挂号保存
export function addRegisterAndPatient(data) {
  return request({
    url: "/finance/currentRegister/add",
    method: "post",
    data: data,
  });
}
