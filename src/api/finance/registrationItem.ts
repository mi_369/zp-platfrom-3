import request from "@/utils/request";

// 查询挂号明细列表 (分页)
export function selectPageRegistrationItem(query) {
  return request({
    url: "/finance/registrationItem/selectPage",
    method: "get",
    params: query,
  });
}

// 查询挂号明细列表 (不分页)
export function listRegistrationItem(query) {
  return request({
    url: "/finance/registrationItem/list",
    method: "get",
    params: query,
  });
}

// 查询挂号明细详细
export function getRegistrationItem(id) {
  return request({
    url: "/finance/registrationItem/" + id,
    method: "get",
  });
}

// 新增挂号明细
export function addRegistrationItem(data) {
  return request({
    url: "/finance/registrationItem",
    method: "post",
    data: data,
  });
}

// 修改挂号明细
export function updateRegistrationItem(data) {
  return request({
    url: "/finance/registrationItem",
    method: "put",
    data: data,
  });
}

// 删除挂号明细
export function delRegistrationItem(id) {
  return request({
    url: "/finance/registrationItem/" + id,
    method: "delete",
  });
}
