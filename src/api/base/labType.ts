import request from '@/utils/request'

// 查询检验项目类型lab_type列表 (分页)
export function selectPageLabType(query) {
    return request({
        url: '/base/labType/selectPage',
        method: 'get',
        params: query
    })
}

// 查询检验项目类型lab_type列表 (不分页)
export function listLabType(query) {
    return request({
        url: '/base/labType/list',
        method: 'get',
        params: query
    })
}

// 查询检验项目类型lab_type详细
export function getLabType(labTypeId) {
    return request({
        url: '/base/labType/' + labTypeId,
        method: 'get'
    })
}

// 新增检验项目类型lab_type
export function addLabType(data) {
    return request({
        url: '/base/labType',
        method: 'post',
        data: data
    })
}

// 修改检验项目类型lab_type
export function updateLabType(data) {
    return request({
        url: '/base/labType',
        method: 'put',
        data: data
    })
}

// 删除检验项目类型lab_type
export function delLabType(labTypeId) {
    return request({
        url: '/base/labType/' + labTypeId,
        method: 'delete'
    })
}
