import request from "@/utils/request";

// 查询数据，ICD-10编码列表 (分页)
export function selectPageHisDataIcd10(query) {
  return request({
    url: "/base/hisDataIcd10/selectPage",
    method: "get",
    params: query,
  });
}

// 查询数据，ICD-10编码列表 (不分页)
export function listHisDataIcd10(query) {
  return request({
    url: "/base/hisDataIcd10/list",
    method: "get",
    params: query,
  });
}

// 查询数据，ICD-10编码详细
export function getHisDataIcd10(id) {
  return request({
    url: "/base/hisDataIcd10/" + id,
    method: "get",
  });
}

// 新增数据，ICD-10编码
export function addHisDataIcd10(data) {
  return request({
    url: "/base/hisDataIcd10",
    method: "post",
    data: data,
  });
}

// 修改数据，ICD-10编码
export function updateHisDataIcd10(data) {
  return request({
    url: "/base/hisDataIcd10",
    method: "put",
    data: data,
  });
}

// 删除数据，ICD-10编码
export function delHisDataIcd10(id) {
  return request({
    url: "/base/hisDataIcd10/" + id,
    method: "delete",
  });
}
// icd10字典检索
export function getAutoHisIcd(query) {
  return request({
      url: '/base/hisDataIcd10/getAutoHisIcd',
      method: 'get',
      params: query
  })
}