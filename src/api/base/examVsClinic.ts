import request from '@/utils/request'

// 查询检查项目与诊疗项目对照列表 (分页)
export function selectPageClinic(query) {
    return request({
        url: '/base/examVsClinic/selectPage',
        method: 'get',
        params: query
    })
}

// 查询检查项目与诊疗项目对照列表 (不分页)
export function listClinic(query) {
    return request({
        url: '/base/examVsClinic/list',
        method: 'get',
        params: query
    })
}

// 查询检查项目与诊疗项目对照详细
export function getClinic(id) {
    return request({
        url: '/base/examVsClinic/' + id,
        method: 'get'
    })
}

// 新增检查项目与诊疗项目对照
export function addClinic(data) {
    return request({
        url: '/base/examVsClinic',
        method: 'post',
        data: data
    })
}

// 修改检查项目与诊疗项目对照
export function updateClinic(data) {
    return request({
        url: '/base/examVsClinic',
        method: 'put',
        data: data
    })
}

// 删除检查项目与诊疗项目对照
export function delClinic(id) {
    return request({
        url: '/base/examVsClinic/' + id,
        method: 'delete'
    })
}
