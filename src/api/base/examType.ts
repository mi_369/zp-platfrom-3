import request from "@/utils/request";

// 查询检查项目类型exam_type列表 (分页)
export function selectPageExamType(query) {
  return request({
    url: "/base/examType/selectPage",
    method: "get",
    params: query,
  });
}

// 查询检查项目类型exam_type列表 (不分页)
export function listExamType(query) {
  return request({
    url: "/base/examType/list",
    method: "get",
    params: query,
  });
}

// 查询检查项目类型exam_type详细
export function getExamType(examTypeId) {
  return request({
    url: "/base/examType/" + examTypeId,
    method: "get",
  });
}

// 新增检查项目类型exam_type
export function addExamType(data) {
  return request({
    url: "/base/examType",
    method: "post",
    data: data,
  });
}

// 修改检查项目类型exam_type
export function updateExamType(data) {
  return request({
    url: "/base/examType",
    method: "put",
    data: data,
  });
}

// 删除检查项目类型exam_type
export function delExamType(examTypeId) {
  return request({
    url: "/base/examType/" + examTypeId,
    method: "delete",
  });
}

/**
 * 刷新缓存
 * @param deptId
 * @returns {*}
 */
export function refreshCacheExamType() {
  return request({
    url: "/base/examType/refreshCacheExamType",
    method: "get",
  });
}
