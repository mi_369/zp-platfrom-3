import request from '@/utils/request'

// 查询用药途径列表 (分页)
export function selectPageAdministrationDict (query) {
    return request({
        url: '/base/administrationDict/selectPage',
        method: 'get',
        params: query
    })
}

// 查询用药途径列表 (不分页)
export function listAdministrationDict (query) {
    return request({
        url: '/base/administrationDict/list',
        method: 'get',
        params: query
    })
}

// 查询用药途径详细
export function getAdministrationDict (id) {
    return request({
        url: '/base/administrationDict/' + id,
        method: 'get'
    })
}

// 新增用药途径
export function addAdministrationDict (data) {
    return request({
        url: '/base/administrationDict',
        method: 'post',
        data: data
    })
}

// 修改用药途径
export function updateAdministrationDict (data) {
    return request({
        url: '/base/administrationDict',
        method: 'put',
        data: data
    })
}

// 删除用药途径
export function delAdministrationDict (id) {
    return request({
        url: '/base/administrationDict/' + id,
        method: 'delete'
    })
}

// 查询系统平台字典列表(业务字典中不存在数据)
export function listNotExistBaseData (query) {
    return request({
        url: '/base/administrationDict/listNotExistBaseData',
        method: 'get',
        params: query
    })
}

// 通过字典勾选批量增加
export function saveBatchAdministrationDictInData (data) {
    return request({
        url: '/base/administrationDict/saveBatchAdministrationDictInData',
        method: 'post',
        data: data
    })
}
