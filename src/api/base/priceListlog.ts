import request from '@/utils/request'

// 查询价表操作记录列表 (分页)
export function selectPagePriceListlog (query) {
    return request({
        url: '/base/priceListlog/selectPage',
        method: 'get',
        params: query
    })
}

// 查询价表操作记录列表 (不分页)
export function listPriceListlog (query) {
    return request({
        url: '/base/priceListlog/list',
        method: 'get',
        params: query
    })
}

// 查询价表操作记录详细
export function getPriceListlog (priceLogId) {
    return request({
        url: '/base/priceListlog/' + priceLogId,
        method: 'get'
    })
}

// 新增价表操作记录
export function addPriceListlog (data) {
    return request({
        url: '/base/priceListlog',
        method: 'post',
        data: data
    })
}

// 修改价表操作记录
export function updatePriceListlog (data) {
    return request({
        url: '/base/priceListlog',
        method: 'put',
        data: data
    })
}

// 删除价表操作记录
export function delPriceListlog (priceLogId) {
    return request({
        url: '/base/priceListlog/' + priceLogId,
        method: 'delete'
    })
}
