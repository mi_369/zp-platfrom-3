import request from "@/utils/request";

// 查询pdf打印模板字典列表 (分页)
export function selectPagePrintPdfTemplate(query) {
  return request({
    url: "/base/printPdfTemplate/selectPage",
    method: "get",
    params: query,
  });
}

// 查询pdf打印模板字典列表 (不分页)
export function listPrintPdfTemplate(query) {
  return request({
    url: "/base/printPdfTemplate/list",
    method: "get",
    params: query,
  });
}

// 查询pdf打印模板字典详细
export function getPrintPdfTemplate(id) {
  return request({
    url: "/base/printPdfTemplate/" + id,
    method: "get",
  });
}

// 新增pdf打印模板字典
export function addPrintPdfTemplate(data) {
  return request({
    url: "/base/printPdfTemplate",
    method: "post",
    data: data,
  });
}

// 修改pdf打印模板字典
export function updatePrintPdfTemplate(data) {
  return request({
    url: "/base/printPdfTemplate",
    method: "put",
    data: data,
  });
}

// 删除pdf打印模板字典
export function delPrintPdfTemplate(id) {
  return request({
    url: "/base/printPdfTemplate/" + id,
    method: "delete",
  });
}

// 文件上传
export function uploadPdfFile(data) {
  return request({
    url: "/base/printPdfTemplate/uploadPdfFile",
    method: "post",
    data: data,
  });
}

// 文件下载
export function downloadFile(data) {
    return request({
        url: '/base/printPdfTemplate/downloadFile',
        method: 'post',
        // mask: true,
        responseType: 'blob',
        headers: {'Content-Type': 'multipart/form-data'},
        data
    })
}