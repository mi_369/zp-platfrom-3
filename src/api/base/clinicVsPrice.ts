import request from '@/utils/request'

// 查询诊疗项目与价对照列表 (分页)
export function selectPageClinicVsPrice (query) {
    return request({
        url: '/base/clinicVsPrice/selectPage',
        method: 'get',
        params: query
    })
}

// 查询诊疗项目与价对照列表 (不分页)
export function listClinicVsPrice (query) {
    return request({
        url: '/base/clinicVsPrice/list',
        method: 'get',
        params: query
    })
}

// 查询诊疗项目与价对照详细
export function getClinicVsPrice (clinicItemId) {
    return request({
        url: '/base/clinicVsPrice/' + clinicItemId,
        method: 'get'
    })
}

// 新增诊疗项目与价对照
export function addClinicVsPrice (data) {
    return request({
        url: '/base/clinicVsPrice',
        method: 'post',
        data: data
    })
}

// 修改诊疗项目与价对照
export function updateClinicVsPrice (data) {
    return request({
        url: '/base/clinicVsPrice',
        method: 'put',
        data: data
    })
}

// 删除诊疗项目与价对照
export function delClinicVsPrice (data) {
    return request({
        url: '/base/clinicVsPrice/delClinicVsPrice',
        method: 'post',
        data: data
    })
}
