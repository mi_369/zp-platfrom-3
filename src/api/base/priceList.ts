import request from '@/utils/request'

// 查询价表列表 (分页)
export function selectPagePriceList (query) {
    return request({
        url: '/base/priceList/selectPage',
        method: 'get',
        params: query
    })
}

// 查询价表列表 (不分页)
export function listPriceList (query) {
    return request({
        url: '/base/priceList/list',
        method: 'get',
        params: query
    })
}

// 查询价表详细
export function getPriceList (priceId) {
    return request({
        url: '/base/priceList/' + priceId,
        method: 'get'
    })
}

// 新增价表
export function addPriceList (data) {
    return request({
        url: '/base/priceList',
        method: 'post',
        data: data
    })
}

// 修改价表
export function updatePriceList (data) {
    return request({
        url: '/base/priceList',
        method: 'put',
        data: data
    })
}

// 删除价表
export function delPriceList (priceId) {
    return request({
        url: '/base/priceList/' + priceId,
        method: 'delete'
    })
}

// 停价
export function stopPrice (priceId) {
    return request({
        url: '/base/priceList/stopPrice/' + priceId,
        method: 'get',
    })
}

// 调价
export function priceAdjustment (data) {
    return request({
        url: '/base/priceList/priceAdjustment',
        method: 'post',
        data: data
    })
}

// 启动
export function priceEnable (data) {
    return request({
        url: '/base/priceList/priceEnable',
        method: 'post',
        data: data
    })
}

// 当前价表自动补全
export function getCurrentPriceList (data) {
    return request({
        url: '/base/priceList/getCurrentPriceList',
        method: 'post',
        data: data
    })
}

// 获取诊疗项目已对照的价表
export function selectContrastExist (data) {
    return request({
        url: '/base/priceList/selectContrastExist',
        method: 'post',
        data: data
    })
}
