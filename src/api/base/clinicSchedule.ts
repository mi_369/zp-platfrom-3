import request from '@/utils/request'

// 查询出诊安排列表 (分页)
export function selectPageClinicSchedule (query) {
    return request({
        url: '/base/clinicSchedule/selectPage',
        method: 'get',
        params: query
    })
}

// 查询出诊安排列表 (不分页)
export function listClinicSchedule (query) {
    return request({
        url: '/base/clinicSchedule/list',
        method: 'get',
        params: query
    })
}

// 查询出诊安排详细
export function getClinicSchedule (scheduleId) {
    return request({
        url: '/base/clinicSchedule/' + scheduleId,
        method: 'get'
    })
}

// 新增出诊安排
export function addClinicSchedule (data) {
    return request({
        url: '/base/clinicSchedule',
        method: 'post',
        data: data
    })
}

// 修改出诊安排
export function updateClinicSchedule (data) {
    return request({
        url: '/base/clinicSchedule',
        method: 'put',
        data: data
    })
}

// 删除出诊安排
export function delClinicSchedule (scheduleId) {
    return request({
        url: '/base/clinicSchedule/' + scheduleId,
        method: 'delete'
    })
}
