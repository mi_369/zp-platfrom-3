import request from '@/utils/request'

// 查询诊断类型、手术字典类型配置列表 (分页)
export function selectPageOperationDiagnosisConfig (query) {
    return request({
        url: '/base/operationDiagnosisConfig/selectPage',
        method: 'get',
        params: query
    })
}

// 查询诊断类型、手术字典类型配置列表 (不分页)
export function listOperationDiagnosisConfig (query) {
    return request({
        url: '/base/operationDiagnosisConfig/list',
        method: 'get',
        params: query
    })
}

// 查询诊断类型、手术字典类型配置详细
export function getOperationDiagnosisConfig (orgId) {
    return request({
        url: '/base/operationDiagnosisConfig/' + orgId,
        method: 'get'
    })
}

// 新增诊断类型、手术字典类型配置
export function addOperationDiagnosisConfig (data) {
    return request({
        url: '/base/operationDiagnosisConfig',
        method: 'post',
        data: data
    })
}

// 修改诊断类型、手术字典类型配置
export function updateOperationDiagnosisConfig (data) {
    return request({
        url: '/base/operationDiagnosisConfig',
        method: 'put',
        data: data
    })
}

// 删除诊断类型、手术字典类型配置
export function delOperationDiagnosisConfig (orgId) {
    return request({
        url: '/base/operationDiagnosisConfig/' + orgId,
        method: 'delete'
    })
}
