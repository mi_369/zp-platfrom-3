import request from '@/utils/request'

// 查询手术基础字典多个类型列表 (分页)
export function selectPageOperationDictIcd10 (query) {
    return request({
        url: '/base/operationDictIcd10/selectPage',
        method: 'get',
        params: query
    })
}

// 查询手术基础字典多个类型列表 (不分页)
export function listOperationDictIcd10 (query) {
    return request({
        url: '/base/operationDictIcd10/list',
        method: 'get',
        params: query
    })
}

// 查询手术基础字典多个类型详细
export function getOperationDictIcd10 (operationDictId) {
    return request({
        url: '/base/operationDictIcd10/' + operationDictId,
        method: 'get'
    })
}

// 新增手术基础字典多个类型
export function addOperationDictIcd10 (data) {
    return request({
        url: '/base/operationDictIcd10',
        method: 'post',
        data: data
    })
}

// 修改手术基础字典多个类型
export function updateOperationDictIcd10 (data) {
    return request({
        url: '/base/operationDictIcd10',
        method: 'put',
        data: data
    })
}

// 删除手术基础字典多个类型
export function delOperationDictIcd10 (operationDictId) {
    return request({
        url: '/base/operationDictIcd10/' + operationDictId,
        method: 'delete'
    })
}
