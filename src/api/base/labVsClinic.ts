import request from '@/utils/request'

// 查询检验项目与诊疗项目对照列表 (分页)
export function selectPageLabVsClinic(query) {
    return request({
        url: '/base/labVsClinic/selectPage',
        method: 'get',
        params: query
    })
}

// 查询检验项目与诊疗项目对照列表 (不分页)
export function listLabVsClinic(query) {
    return request({
        url: '/base/labVsClinic/list',
        method: 'get',
        params: query
    })
}

// 查询检验项目与诊疗项目对照详细
export function getLabVsClinic(id) {
    return request({
        url: '/base/labVsClinic/' + id,
        method: 'get'
    })
}

// 新增检验项目与诊疗项目对照
export function addLabVsClinic(data) {
    return request({
        url: '/base/labVsClinic',
        method: 'post',
        data: data
    })
}

// 修改检验项目与诊疗项目对照
export function updateLabVsClinic(data) {
    return request({
        url: '/base/labVsClinic',
        method: 'put',
        data: data
    })
}

// 删除检验项目与诊疗项目对照
export function delLabVsClinic(id) {
    return request({
        url: '/base/labVsClinic/' + id,
        method: 'delete'
    })
}
