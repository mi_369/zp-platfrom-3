import request from '@/utils/request'

// 查询手术操作字典列表 (分页)
export function selectPageOperationDict (query) {
    return request({
        url: '/base/operationDict/selectPage',
        method: 'get',
        params: query
    })
}

// 查询手术操作字典列表 (不分页)
export function listOperationDict (query) {
    return request({
        url: '/base/operationDict/list',
        method: 'get',
        params: query
    })
}

// 查询手术操作字典详细
export function getOperationDict (operationDictId) {
    return request({
        url: '/base/operationDict/' + operationDictId,
        method: 'get'
    })
}

// 新增手术操作字典
export function addOperationDict (data) {
    return request({
        url: '/base/operationDict',
        method: 'post',
        data: data
    })
}

// 修改手术操作字典
export function updateOperationDict (data) {
    return request({
        url: '/base/operationDict',
        method: 'put',
        data: data
    })
}

// 删除手术操作字典
export function delOperationDict (operationDictId) {
    return request({
        url: '/base/operationDict/' + operationDictId,
        method: 'delete'
    })
}

//手术项目自动补全
export function getOperationAuto (data) {
    return request({
        url: '/base/operationDict/getOperationAuto',
        method: 'post',
        data: data
    })
}
