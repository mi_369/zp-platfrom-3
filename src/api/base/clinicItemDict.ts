import request from '@/utils/request'

// 查询诊疗项目列表 (分页)
export function selectPageClinicItemDict (query) {
    return request({
        url: '/base/clinicItemDict/selectPage',
        method: 'get',
        params: query
    })
}

// 查询诊疗项目列表 (不分页) || 诊疗项目自动补全
export function listClinicItemDict (query) {
    return request({
        url: '/base/clinicItemDict/list',
        method: 'get',
        params: query
    })
}

// 查询诊疗项目详细
export function getClinicItemDict (clinicItemId) {
    return request({
        url: '/base/clinicItemDict/' + clinicItemId,
        method: 'get'
    })
}

// 新增诊疗项目
export function addClinicItemDict (data) {
    return request({
        url: '/base/clinicItemDict',
        method: 'post',
        data: data
    })
}

// 修改诊疗项目
export function updateClinicItemDict (data) {
    return request({
        url: '/base/clinicItemDict',
        method: 'put',
        data: data
    })
}

// 删除诊疗项目
export function delClinicItemDict (clinicItemId) {
    return request({
        url: '/base/clinicItemDict/' + clinicItemId,
        method: 'delete'
    })
}
