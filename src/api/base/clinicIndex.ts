import request from '@/utils/request'

// 查询号别列表 (分页)
export function selectPageClinicIndex (query) {
    return request({
        url: '/base/clinicIndex/selectPage',
        method: 'get',
        params: query
    })
}

// 查询号别列表 (不分页)
export function listClinicIndex (query) {
    return request({
        url: '/base/clinicIndex/list',
        method: 'get',
        params: query
    })
}

// 查询号别详细
export function getClinicIndex (clinicIndexId) {
    return request({
        url: '/base/clinicIndex/' + clinicIndexId,
        method: 'get'
    })
}

// 新增号别
export function addClinicIndex (data) {
    return request({
        url: '/base/clinicIndex',
        method: 'post',
        data: data
    })
}

// 修改号别
export function updateClinicIndex (data) {
    return request({
        url: '/base/clinicIndex',
        method: 'put',
        data: data
    })
}

// 删除号别
export function delClinicIndex (clinicIndexId) {
    return request({
        url: '/base/clinicIndex/' + clinicIndexId,
        method: 'delete'
    })
}
