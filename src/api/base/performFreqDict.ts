import request from '@/utils/request'

// 查询执行频次列表 (分页)
export function selectPagePerformFreqDict (query) {
    return request({
        url: '/base/performFreqDict/selectPage',
        method: 'get',
        params: query
    })
}

// 查询执行频次列表 (不分页)
export function listPerformFreqDict (query) {
    return request({
        url: '/base/performFreqDict/list',
        method: 'get',
        params: query
    })
}

// 查询执行频次详细
export function getPerformFreqDict (freqId) {
    return request({
        url: '/base/performFreqDict/' + freqId,
        method: 'get'
    })
}

// 新增执行频次
export function addPerformFreqDict (data) {
    return request({
        url: '/base/performFreqDict',
        method: 'post',
        data: data
    })
}

// 修改执行频次
export function updatePerformFreqDict (data) {
    return request({
        url: '/base/performFreqDict',
        method: 'put',
        data: data
    })
}

// 删除执行频次
export function delPerformFreqDict (freqId) {
    return request({
        url: '/base/performFreqDict/' + freqId,
        method: 'delete'
    })
}
