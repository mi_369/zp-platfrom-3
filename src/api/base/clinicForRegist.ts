import request from '@/utils/request'

// 查询号表安排列表 (分页)
export function selectPageClinicForRegist(query) {
    return request({
        url: '/base/clinicForRegist/selectPage',
        method: 'post',
        data: query
    })
}

// 查询号表安排列表 (不分页)
export function listClinicForRegist(query) {
    return request({
        url: '/base/clinicForRegist/list',
        method: 'get',
        params: query
    })
}

// 查询号表安排详细
export function getClinicForRegist(clinicForRegistId) {
    return request({
        url: '/base/clinicForRegist/' + clinicForRegistId,
        method: 'get'
    })
}

// 新增号表安排
export function addClinicForRegist(data) {
    return request({
        url: '/base/clinicForRegist',
        method: 'post',
        data: data
    })
}

// 修改号表安排
export function updateClinicForRegist(data) {
    return request({
        url: '/base/clinicForRegist',
        method: 'put',
        data: data
    })
}

// 删除号表安排
export function delClinicForRegist(clinicForRegistId) {
    return request({
        url: '/base/clinicForRegist/' + clinicForRegistId,
        method: 'delete'
    })
}
