const defaultSettings: AppSettings = {
  title: "后台管理",
  version: "v2.7.0",
  showSettings: true,
  tagsView: true,
  fixedHeader: false,
  sidebarLogo: true,
  layout: "left",
  theme: "light",
  // 'large' | 'default' | 'small'  '大'|'默认'|'小'
  size: "small",
  language: "zh-cn",
  themeColor: "theme-dark",
  watermark: { enabled: false, content: "zp" },
};

export default defaultSettings;
