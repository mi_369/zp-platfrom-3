import type { App } from "vue";
import { hasPerm } from "./permission/hasPerm";
import { adaptive } from "./v-adaptive";

export function setupDirective(app: App<Element>) {
  // 按钮、字符串权限
  app.directive("hasPerm", hasPerm);
  // 表格自适应高度
  app.directive("adaptive", adaptive);
}

/**
 自定义指令中的生命周期钩子函数
一个指令定义对象可以提供如下几个钩子函数（都是可选的，根据需要引入）

created ：绑定元素属性或事件监听器被应用之前调用。该指令需要附加需要在普通的 v-on 事件监听器前调用的事件监听器时，这很有用。

beforeMounted ：当指令第一次绑定到元素并且在挂载父组件之前执行。

mounted ：绑定元素的父组件被挂载之后调用。

beforeUpdate ：在更新包含组件的 VNode 之前调用。

updated ：在包含组件的 VNode 及其子组件的 VNode 更新后调用。

beforeUnmounted ：在卸载绑定元素的父组件之前调用

unmounted ：当指令与元素解除绑定且父组件已卸载时，只调用一次。

作者：前端人_倩倩
链接：https://www.jianshu.com/p/d2e5db43f1d3
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
