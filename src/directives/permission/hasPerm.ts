/**
 * v-hasPerm 操作权限处理
 * Copyright (c) 2019 zhang peng
 */

import { useUserStoreHook } from "@/store/modules/user";
import { Directive, DirectiveBinding } from "vue";
const useUserStore = useUserStoreHook();

export const hasPerm: Directive = {
  mounted(el: HTMLElement, binding: DirectiveBinding) {
    const { value } = binding;
    const all_permission = "*:*:*";
    const permissions = useUserStore && useUserStore.user.permissions;
    if (value && value instanceof Array && value.length > 0) {
      const permissionFlag = value;

      const hasPermssions = permissions.some((permission) => {
        return (
          all_permission === permission || permissionFlag.includes(permission)
        );
      });

      if (!hasPermssions) {
        el.parentNode && el.parentNode.removeChild(el);
      }
    } else {
      throw new Error(`请设置操作权限标签值`);
    }
  },
};
