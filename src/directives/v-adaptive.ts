import { Directive, DirectiveBinding } from "vue";
import { ElTable, ElTableColumn } from "element-plus";
export const adaptive: Directive = {
  mounted(el: HTMLElement, binding: DirectiveBinding) {
    // 需要减去的高度，值越大离底部越高 用指令v-adaptive="60"设置
    let bottomHeight = 70;
    if (typeof (binding.value) !== 'undefined') {
      bottomHeight = binding.value;
    }
    const height =
      window.innerHeight - el.getBoundingClientRect().top - bottomHeight;
    el.style.height = height + "px";
    window.addEventListener("resize", () => {
      const height =
        window.innerHeight - el.getBoundingClientRect().top - bottomHeight;
      el.style.height = height + "px";
    });
  },
};

/*
  el：指令所绑定的元素，可以用来直接操作 DOM。
  binding：一个对象，包含以下 property：
  name：指令名，不包括 v- 前缀。
  value：指令的绑定值，例如：v-my-directive=“1 + 1” 中，绑定值为 2。
  oldValue：指令绑定的前一个值，仅在 update 和 componentUpdated 钩子中可用。无论值是否改变都可用。
  expression：字符串形式的指令表达式。例如 v-my-directive=“1 + 1” 中，表达式为 “1 + 1”。
  arg：传给指令的参数，可选。例如 v-my-directive:foo 中，参数为 “foo”。
  modifiers：一个包含修饰符的对象。例如：v-my-directive.foo.bar 中，修饰符对象为 { foo: true, bar: true }。
  vnode：Vue 编译生成的虚拟节点。移步 VNode API 来了解更多详情。
  oldVnode：上一个虚拟节点，仅在 update 和 componentUpdated 钩子中可用。
*/
