# zp-platfrom-3

#### Description
{**When you're done, you can delete the content in this README and update the file with details for others getting started with your repository**}

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
    ![img.png](img.png)

父组件通过ref调用子组件定义的方法 
    子组件中将需要调用的方法通过defineExpose 导出
defineExpose({
    getPersonnelAuthorityDept
})
自定义svg标签用法
<svg-icon :icon-class="'readCard'" />

拼音码
word：必填。String 类型，需要转化为拼音的中文
options：可选。Object 类型，用于配置各种输出形式，options 的键值配置如下：

参数                说明                                                                类型      可选值                                      默认值
pattern             输出的结果的信息 (拼音/声母/母/音/首字母)                           string      pinyin / initial / final / num / first      pinyin
toneType            音调输出形式(拼音符号/数字/不加音调)                                string      symbol / num / none                         symbol
type                输出结果类型 (字符串/数组)                                         string      string / array                              string
multiple            输出多音字全部拼音 (仅在 word 为长度为 1的汉字字符串时生效)          boolean      true / false                                false
mode                拼音查找的模式(常规模式/姓氏模式)                                   string      normal / surname                            normal
