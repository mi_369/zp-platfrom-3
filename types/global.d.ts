declare global {
  /**
   * 分页查询参数
   */
  export interface PageQuery {
    pageNum?: number;
    pageSize?: number;
  }

  /**
   * 分页响应对象
   */
  interface PageResult<T> {
    /** 数据列表 */
    list: T;
    /** 总数 */
    total: number;
  }

  /**
   * 页签对象
   */
  interface TagView {
    /** 页签名称 */
    name: string;
    /** 页签标题 */
    title: string;
    /** 页签路由路径 */
    path: string;
    /** 页签路由完整路径 */
    fullPath: string;
    /** 页签图标 */
    icon?: string;
    /** 是否固定页签 */
    affix?: boolean;
    /** 是否开启缓存 */
    keepAlive?: boolean;
    /** 路由查询参数 */
    query?: any;
  }

  /**
   * 系统设置
   */
  interface AppSettings {
    /** 系统标题 */
    title: string;
    /** 系统版本 */
    version: string;
    /** 是否显示设置 */
    showSettings: boolean;
    /** 是否固定头部 */
    fixedHeader: boolean;
    /** 是否显示多标签导航 */
    tagsView: boolean;
    /** 是否显示侧边栏Logo */
    sidebarLogo: boolean;
    /** 导航栏布局(left|top|mix) */
    layout: string;
    /** 主题颜色 */
    themeColor: string;
    /** 主题模式(dark|light) */
    theme: string;
    /** 布局大小(default |large |small) */
    size: string;
    /** 语言( zh-cn| en) */
    language: string;
    /** 水印配置 */
    watermark: {
      /** 是否开启水印 */
      enabled: boolean;
      /** 水印内容 */
      content: string;
    };
  }

  /**
   * 组件数据源
   */
  interface OptionType {
    /** 值 */
    value: string | number;
    /** 文本 */
    label: string;
    /** 子列表  */
    children?: OptionType[];
  }

  /**
   * 菜单表单对象类型
   */
  export interface MenuForm {
    /**
     * 菜单ID
     */
    menuId?: string;
    /**
     * 父菜单ID
     */
    parentId?: number;
    /**
     * 菜单名称
     */
    menuName?: string;
    /**
     * 菜单是否可见(1:是;0:否;)
     */
    visible: string;

    /**
     * 菜单状态（0正常 1停用）
     */
    status: string;

    isFrame?: string;

    icon?: string;
    /**
     * 排序
     */
    orderNum: number;
    /**
     * 组件路径
     */
    component?: string;
    /**
     * 路由路径
     */
    path?: string;
    /**
     * 跳转路由路径
     */
    // redirect?: string;

    /**
     * 菜单类型
     */
    menuType: string;

    /**
     * 权限标识
     */
    perms?: string;
    /**
     * 【菜单】是否开启页面缓存
     */
    isCache?: string;

    /**
     * 路由参数
     */
    queryParam?: string

    /**
     * 【目录】只有一个子路由是否始终显示
     */
    // alwaysShow?: number;
  }
}
export {};
