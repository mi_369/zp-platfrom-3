import { number } from "echarts";
import { saveAs } from "file-saver";
declare global {
  /** 用户userInfo */
  export interface UserInfoBusi {
    searchValue?: string;
    createBy: string;
    createTime: object;
    updateBy: string;
    updateTime: object;
    params: object;
    itemInfo: string;
    notInSelect: string;
    isFiltrationContrastExist: string;
    exceptionCode: string;
    userId: string;
    deptId: string;
    userName: string;
    nickName: string;
    userType: string;
    email: string;
    phonenumber: string;
    sex: string;
    avatar: string;
    status: string;
    delFlag: string;
    loginIp: string;
    loginDate: object;
    remark: string;
    orgId: string;
    loginDeptId: string;
    customerType: string;
    certificateType: string;
    certificateNo: string;
    medicalCode: string;
    loginDeptName: string;
    orgName: string;
    roles: any[];
    roleIds: string;
    postIds: string;
    roleId: string;
    sysRoleGroupList: any[];
    deptGroupList: any[];
    deptGroupId: string;
    admin: boolean;
    dept: Dept;
  }
  /** 科室信息 */
  export interface Dept {
    searchValue?: string;
    createBy?: string;
    createTime?: object;
    updateBy?: string;
    updateTime?: object;
    params?: Map;
    itemInfo?: string;
    notInSelect?: string;
    isFiltrationContrastExist?: string;
    exceptionCode?: string;
    parentName?: string;
    parentId?: string;
    children?: any[Dept];
    deptId: string;
    deptName: string;
    deptCode: string;
    clinicalSign?: string;
    deptAttribute?: string;
    clinicOrVisit?: string;
    internalOrSurgery?: string;
    placeYard?: string;
    medicalInsurance?: string;
    orderNum?: number;
    leader?: string;
    phone?: string;
    email?: string;
    status?: string;
    delFlag?: string;
    ancestors?: string;
    orgId?: string;
    orgName?: string;
    clinicVsVisitArray?: string[];
    deptAttributeArray?: string[];
  }
  export interface DictData {
    dictCode?: string;
    dictSort?: number;
    dictLabel: string;
    dictValue: string;
    dictType: string;
    cssClass?: string;
    listClass?: string;
    status?: string;
  }

  /** 系统用户信息 */
  export interface SysUser extends PageQuery {
    userId?: string;
    customerType?: string;
    certificateType?: string;
    certificateNo?: string;
    deptId?: string;
    userName?: string;
    nickName?: string;
    password?: string;
    phonenumber?: string;
    email?: string;
    sex?: string;
    status?: string;
    remark?: string;
    postIds?: string[];
    roleIds?: string[];
    orgId?: string;
    deptName?: string;
    medicalCode?: string;
  }
  /** 自定义服务信息表 */
  export interface SysRoleGroup extends PageQuery {
    roleGroupId?: string;
    roleGroupName?: string;
    orgId?: string;
    roleKey?: string;
    menuCheckStrictly?: boolean;
    roleSort?: number;
    status?: string;
    remark?: string;
    menuIds?: any;
  }
  export interface PriceList extends PageQuery {
    priceId?: string;
    /**
     * 项目分类
     */
    itemClass?: string;
    /**
     * 项目代码
     */
    itemCode?: string;
    /**
     * 项目名称
     */
    itemName?: string;
    /**
     * 项目规格
     */
    itemSpec?: string;
    /**
     * 医保类型
     */
    medicalInsuranceType?: string;
    /**
     * 单位
     */
    units?: string;
    /**
     * 网络价格
     */
    networkPrice?: number;
    /**
     * 批发价格
     */
    tradePrice?: number;
    /**
     * 价格
     */
    price?: number;
    /**
     * 优惠价格
     */
    preferPrice?: number;
    /**
     * 门诊费用分类
     */
    classOnRcpt?: string;
    /**
     * 住院费用分类
     */
    classOnInp?: string;
    /**
     * 核算项目分类
     */
    classOnReckoning?: string;
    /**
     * 会计科目
     */
    subjCode?: string;
    /**
     * 病案首页分类
     */
    classOnMr?: string;
    /**
     * 开始时间
     */
    startDate: any;
    /**
     * 停止时间
     */
    stopDate?: any;
    /**
     * 拼音码
     */
    inputCode?: string;
    /**
     * 机构ID
     */
    orgId?: string;

    /**
     * 备注信息
     */
    remark?: string;
    /**
     * 医保对照项目代码
     */
    itemCodeYbCompare?: string;
    /**
     * 自费或医保(0自费,1医保)
     */
    zfOrYb?: string;
    // 是否生成诊疗项目
    intoItemCheck?: boolean;
  }
}

export {};
