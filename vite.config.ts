import { defineConfig, loadEnv } from "vite";
import type { UserConfig, ConfigEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import AutoImport from "unplugin-auto-import/vite"; //自动引入ref,reactive等等等
import { resolve } from "path";
import { createSvgIconsPlugin } from "vite-plugin-svg-icons";
import Icons from "unplugin-icons/vite";
import IconsResolver from "unplugin-icons/resolver";
import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import UnoCSS from "unocss/vite";

const pathSrc = (dir: string) => resolve(process.cwd(), dir);

// https://vitejs.dev/config/
export default defineConfig(({ mode }: ConfigEnv): UserConfig => {
  const env = loadEnv(mode, process.cwd());
  return {
    // 部署生产环境和开发环境下的URL。
    // 默认情况下，Vue CLI 会假设你的应用是被部署在一个域名的根路径上
    // 例如 https://www.zhang peng.vip/。如果应用被部署在一个子路径上，你就需要用这个选项指定这个子路径。例如，如果你的应用被部署在 https://www.zhang peng.vip/admin/，则设置 baseUrl 为 /admin/。
    // publicPath:
    // process.env.NODE_ENV === "production" ? "/production-sub-path/" : "/",
    // publicPath: process.env.VUE_APP_CONTEXT_PATH,
    // 在npm run build 或 yarn build 时 ，生成文件的目录名称（要和baseUrl的生产环境路径一致）（默认dist）
    // outputDir: "dist",
    // 用于放置生成的静态资源 (js、css、img、fonts) 的；（项目打包之后，静态资源会放在这个文件夹下）
    // assetsDir: "static",
    // 是否开启eslint保存检测，有效值：ture | false | 'error'
    // lintOnSave: process.env.NODE_ENV === "development",
    // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
    // productionSourceMap: false,
    // transpileDependencies: true,
    // 解决跨域
    // devServer: {
    //   port: process.env.VUE_APP_SERVICE_PORT, // 端口号
    //   open: true, // 配置自动启动浏览器
    //   // hotOnly: true, // 热更新
    //   proxy: {
    //     // detail: https://cli.vuejs.org/config/#devserver-proxy
    //     [process.env.VUE_APP_BASE_API]: {
    //       target: "http://localhost:8080",
    //       changeOrigin: true,
    //       pathRewrite: {
    //         ["^" + process.env.VUE_APP_BASE_API]: "",
    //       },
    //     },
    //   },
    // },
    // css: {
    //   // CSS 预处理器
    //   preprocessorOptions: {
    //     // 定义全局 SCSS 变量
    //     scss: {
    //       javascriptEnabled: true,
    //       additionalData: `
    //         @use "@/styles/variables.module.scss" as *;
    //       `,
    //     },
    //   },
    // },
    server: {
      // 允许IP访问
      host: "0.0.0.0",
      // 应用端口 (默认:3000)
      port: Number(env.VITE_APP_PORT),
      strictPort: false, // 端口被占用直接退出
      // 运行是否自动打开浏览器
      open: true,
      proxy: {
        /**
         * 反向代理解决跨域配置
         * http://localhost:3000/dev-api/users (F12可见请求路径) => http://localhost:8989/users (实际请求后端 API 路径)
         *
         * env.VITE_APP_BASE_API: /dev-api
         * env.VITE_APP_API_URL: http://localhost:8989
         */
        [env.VUE_APP_BASE_API]: {
          changeOrigin: true,
          target: env.VITE_APP_API_URL,
          rewrite: (path) =>
            path.replace(new RegExp("^" + env.VUE_APP_BASE_API), ""),
        },
      },
    },
    plugins: [
      vue(),
      // UnoCSS({
      //   hmrTopLevelAwait: false,
      // }),
      AutoImport({
        imports: ["vue", "@vueuse/core"],
        dts: "src/auto-import.d.ts",
        // 自动导入 Element Plus 相关函数，如：ElMessage, ElMessageBox... (带样式)
        resolvers: [ElementPlusResolver(), IconsResolver({})],
        eslintrc: {
          enabled: false,
          filepath: "./.eslintrc-auto-import.json",
          globalsPropValue: true,
        },
        vueTemplate: true,
      }),

      Components({
        resolvers: [
          // 自动导入 Element Plus 组件
          ElementPlusResolver(),
          // 自动注册图标组件 @iconify-json/ep 是 Element Plus 的图标库
          IconsResolver({ enabledCollections: ["ep"] }),
        ],
        // 指定自定义组件位置(默认:src/components) 配置后页面不用再单独import 组件
        dirs: ["src/components", "src/**/components"],
        // 配置文件位置 (false:关闭自动生成)
        dts: false,
        // dts: "types/components.d.ts",
      }),
      Icons({
        autoInstall: true,
      }),
      createSvgIconsPlugin({
        // 指定需要缓存的图标文件夹(本地svg、icon图标)
        iconDirs: [pathSrc("src/assets/icons/svg")],
        // 指定symbolId格式
        symbolId: "icon-[dir]-[name]",
      }),
    ],
    resolve: {
      // ↓路径别名
      alias: {
        "@": resolve(__dirname, "./src"),
        _c: resolve(__dirname, "./src/components"),
        "*": resolve(""),
        "vue-i18n": "vue-i18n/dist/vue-i18n.cjs.js",
      },
    },
    // 预加载项目必需的组件
    optimizeDeps: {
      include: [
        "vue",
        "vue-router",
        "pinia",
        "axios",
        "element-plus/es/components/form/style/css",
        "element-plus/es/components/form-item/style/css",
        "element-plus/es/components/button/style/css",
        "element-plus/es/components/input/style/css",
        "element-plus/es/components/input-number/style/css",
        "element-plus/es/components/switch/style/css",
        "element-plus/es/components/upload/style/css",
        "element-plus/es/components/menu/style/css",
        "element-plus/es/components/col/style/css",
        "element-plus/es/components/icon/style/css",
        "element-plus/es/components/row/style/css",
        "element-plus/es/components/tag/style/css",
        "element-plus/es/components/dialog/style/css",
        "element-plus/es/components/loading/style/css",
        "element-plus/es/components/radio/style/css",
        "element-plus/es/components/radio-group/style/css",
        "element-plus/es/components/popover/style/css",
        "element-plus/es/components/scrollbar/style/css",
        "element-plus/es/components/tooltip/style/css",
        "element-plus/es/components/dropdown/style/css",
        "element-plus/es/components/dropdown-menu/style/css",
        "element-plus/es/components/dropdown-item/style/css",
        "element-plus/es/components/sub-menu/style/css",
        "element-plus/es/components/menu-item/style/css",
        "element-plus/es/components/divider/style/css",
        "element-plus/es/components/card/style/css",
        "element-plus/es/components/link/style/css",
        "element-plus/es/components/breadcrumb/style/css",
        "element-plus/es/components/breadcrumb-item/style/css",
        "element-plus/es/components/table/style/css",
        "element-plus/es/components/tree-select/style/css",
        "element-plus/es/components/table-column/style/css",
        "element-plus/es/components/select/style/css",
        "element-plus/es/components/option/style/css",
        "element-plus/es/components/pagination/style/css",
        "element-plus/es/components/tree/style/css",
        "element-plus/es/components/alert/style/css",
        "element-plus/es/components/radio-button/style/css",
        "element-plus/es/components/checkbox-group/style/css",
        "element-plus/es/components/checkbox/style/css",
        "element-plus/es/components/tabs/style/css",
        "element-plus/es/components/tab-pane/style/css",
        "element-plus/es/components/rate/style/css",
        "element-plus/es/components/date-picker/style/css",
        "element-plus/es/components/notification/style/css",
        "element-plus/es/components/image/style/css",
        "element-plus/es/components/statistic/style/css",
        "@vueuse/core",
        "sortablejs",
        // "path-to-regexp",
        "echarts",
        // "@wangeditor/editor",
        // "@wangeditor/editor-for-vue",
        "vue-i18n",
      ],
    },

    // 构建配置
    build: {
      chunkSizeWarningLimit: 2000, // 消除打包大小超过500kb警告
      minify: "terser", // Vite 2.6.x 以上需要配置 minify: "terser", terserOptions 才能生效
      terserOptions: {
        compress: {
          keep_infinity: true, // 防止 Infinity 被压缩成 1/0，这可能会导致 Chrome 上的性能问题
          drop_console: true, // 生产环境去除 console
          drop_debugger: true, // 生产环境去除 debugger
        },
        format: {
          comments: false, // 删除注释
        },
      },
    },
  };
});
